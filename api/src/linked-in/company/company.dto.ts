import { Field, ID, ObjectType } from '@nestjs/graphql';
import { PersonType } from '../person/person.dto';

@ObjectType()
export class CompanyType {
  @Field(() => ID, { description: 'company id' })
  readonly id: string;
  @Field(() => String, {
    description: 'company name',
  })
  name: string;
  @Field(() => String, {
    description: 'company logo',
  })
  logo: string;
  @Field(() => String, { description: 'phone', nullable: true })
  phoneNumber: string;
  @Field((type) => [PersonType], { description: 'company', nullable: true })
  people: [PersonType];
}
