import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { AppUserService } from './app-user.service';
import { AppUserType } from './app-user.dto';
import { AppUser } from './app-user.interface';
import { UpdateAppUser } from './update-app-user';
import { AddAppUser } from './add-app-user';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from '../auth.guard';
import { MD5 } from '../utils';
import { PubSub } from 'graphql-subscriptions';

const pubSub = new PubSub();
@Resolver('AppUser')
export class AppUserResolver {
  constructor(private readonly _appUserService: AppUserService) {}

  @Query(() => [AppUserType])
  @UseGuards(AuthGuard)
  async users(): Promise<AppUserType> {
    return await this._appUserService.getAllAppUser();
  }

  @Subscription(() => AppUserType, {
    resolve: (value) => value,
  })
  @UseGuards(AuthGuard)
  async userUpdate(@Args('id') id: string) {
    return pubSub.asyncIterator('userUpdate');
  }
  @Query(() => AppUserType)
  @UseGuards(AuthGuard)
  async user(@Args('id') id: string): Promise<AppUserType> {
    return await this._appUserService.getAppUser(id);
  }

  @Mutation(() => AppUserType)
  // @UseGuards(AuthGuard)
  async updateUser(
    @Args('id') id: string,
    @Args('appUser') appUser: UpdateAppUser,
  ): Promise<AppUserType> {
    const user = this._appUserService.update(id, appUser);
    await pubSub.publish('userUpdate', user);

    return user;
  }

  @Mutation(() => AppUserType)
  async createUser(@Args('appUser') appUser: AddAppUser): Promise<AppUserType> {
    return await this._appUserService.createAppUser(appUser).then((user) => {
      //TODO! ugly hack
      const _user = Object.assign({}, (user as any)._doc);
      return { ..._user, token: MD5(user.email) };
    });
  }

  @Mutation(() => AppUserType)
  @UseGuards(AuthGuard)
  async deleteAppUser(@Args('id') id: string): Promise<UpdateAppUser> {
    return await this._appUserService.delete(id);
  }

  @Query(() => AppUserType)
  async registeredUser(
    @Args('email') email: string,
    @Args('password') password: string,
  ): Promise<AppUserType | string> {
    return await this._appUserService
      .getRegistredUser(email, password)
      .then((user) => {
        //TODO! ugly hack
        const _user = Object.assign({}, (user as any)._doc);
        return { ..._user, token: MD5(user.email) };
      });
  }
}
