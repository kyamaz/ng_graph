export interface BaseUser {
  username: string;
  id: number;
  token: string;
}

export interface ExtendedUser extends BaseUser {
  email: string;
}

export interface UserGraph {
  id: string;
  userName: number;
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  avatar: string;
}
