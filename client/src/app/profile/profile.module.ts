import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditComponent } from './containers/edit/edit.component';
import { RouterModule } from '@angular/router';
import { ROUTES } from './profile.routes';
import { EditProfileFormComponent } from './components/edit-profile-form/edit-profile-form.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [EditComponent, EditProfileFormComponent],
  imports: [CommonModule, SharedModule, RouterModule.forChild(ROUTES)],
})
// @ts-ignore
export class ProfileModule {}
