export interface ActionType<T> {
  type: string;
  payload: T;
}

export type SortOrder = 'desc' | 'asc';
export type SortKey = '_id' | 'lastName' | 'jobTitle';
export interface SortState {
  key: SortKey;
  order: SortOrder;
}
export * from '@models/./app.model';
export * from '@models/user.model';
