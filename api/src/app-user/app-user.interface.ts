import { Document } from 'mongoose';

export interface AppUser extends Document {
  id: string;
  userName: number;
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  avatar: string;
}
