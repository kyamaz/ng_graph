import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

@Injectable({ providedIn: 'root' })
export class WindowRef {
  constructor(@Inject(PLATFORM_ID) private platformId: Object) {}

  get nativeWindow(): any {
    return _window();
  }

  clearLogStorage(): void {
    if (isPlatformBrowser(this.platformId)) {
      if (!!_window().localStorage.getItem('app')) {
        _window().localStorage.removeItem('app');
      }
    }
  }

  getUrl(): string {
    if (isPlatformBrowser(this.platformId)) {
      if (window != undefined) {
        return (
          _window().location.protocol +
          '//' +
          _window().location.hostname +
          ':' +
          _window().location.port
        );
      }
    }
  }

  getUserLng(): string {
    if (isPlatformBrowser(this.platformId)) {
      return !!_window().localStorage.getItem('app')
        ? _window().localStorage.getItem('LOCALIZE_DEFAULT_LANGUAGE')
        : 'en ';
    }
  }
}

function _window(): any {
  // return the native window obj
  return window;
}
