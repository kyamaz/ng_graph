import { Injectable } from '@angular/core';
import {
  assign,
  EventObject,
  interpret,
  Machine,
  MachineOptions,
} from 'xstate';
import { fromEventPattern, of, Subject } from 'rxjs';
import { catchError, delay, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { LoginType } from '@login/login/login.component';
import { AppService } from '@appServices/app.service';
import { LOCAL_STORAGE_KEY } from '../../shared/const';
import { BaseUser } from '@models/user.model';

interface AuthContext {
  retry: number;
  user: BaseUser;
  error: string;
}

export class Authenticating {
  readonly type = 'SUBMITTING';

  constructor(
    public payload: {
      username: string;
      email: string;
      password: string;
      loginType: LoginType;
    }
  ) {}
}

export class Unauthenticated {
  readonly type = 'UNAUTHENTICATED';

  constructor(public error: any) {}
}

export class LoginError {
  readonly type = 'AUTH_FAILED';

  constructor(public error: any) {}
}

export class LoginSuccess {
  readonly type = 'AUTHENTICATED';
  constructor(public user: BaseUser) {}
}

export type AuthEvent =
  | Authenticating
  | LoginSuccess
  | LoginError
  | Unauthenticated;

type boot = 'boot';

type final = 'final';

interface AuthStateSchema {
  states: {
    boot: {};
    unauthorized: {};
    authorized: {};
    authorizing: {};
    authorizingFailed: {};
  };
}

const authMachineConfig = {
  id: 'authMachine',
  initial: 'boot' as boot,
  context: {
    retry: 0,
    user: null,
    error: null,
  },
  states: {
    boot: {
      on: {
        '': [
          { target: 'unauthorized', cond: 'isLoggedOut' },
          { target: 'authorized' },
        ],
      },
    },
    unauthorized: {
      on: {
        SUBMITTING: {
          target: 'authorizing',
        },
      },
    },
    authorizingFailed: {
      invoke: {
        id: 'authErr',
        src: 'handleErr',
      },
      on: {
        UNAUTHENTICATED: {
          target: 'unauthorized',
          actions: ['assignErrors'],
        },
      },
    },
    authorized: {
      entry: ['loginSuccess'],
      isFinite: true,
    },
    authorizing: {
      invoke: {
        id: 'login',
        src: 'requestAuth',
      },
      on: {
        AUTHENTICATED: {
          target: 'authorized',
          actions: ['assignUser', 'loginSuccess'],
        },
        AUTH_FAILED: { target: 'authorizingFailed' },
      },
    },
  },
};
interface UserAuth {
  email: string;
  password: string;
  username?: string;
  loginType: LoginType;
}
interface LoginPayload {
  payload: UserAuth;
  type: 'SUBMITTING';
}
@Injectable({
  providedIn: 'root',
})
export class AuthMachineService {
  // AuthContext, AuthSchema, AuthEvent
  isDone$: Subject<boolean> = new Subject();
  authMachineOptions: Partial<MachineOptions<AuthContext, AuthEvent>> = {
    services: {
      requestAuth: (_, { payload }: LoginPayload) => {
        return this._handleUserAuth(payload).pipe(
          map((res: any) => {
            const { data } = res;
            const prop =
              payload.loginType === 'signup' ? 'createUser' : 'registeredUser';
            const user = data[prop];
            return new LoginSuccess({
              username: user.userName,
              id: user.id,
              token: user.token,
            });
          }),
          catchError((err) => {
            console.log(err);

            return of(new LoginError('server error whatever'));
          })
        );
      },
      handleErr: (_, event) => {
        return of('error').pipe(
          delay(1000),
          map((res) => {
            return new Unauthenticated('server error');
          })
        );
      },
    },
    actions: {
      assignUser: assign<AuthContext, LoginSuccess>((_, event) => ({
        user: event.user,
      })),
      assignErrors: assign<AuthContext, LoginError>((ctx, event) => {
        return { error: event.error, retry: ctx.retry = ctx.retry + 1 };
      }),
      loginSuccess: (ctx, _) => {
        if (ctx.user) {
          const jsonState = JSON.stringify(ctx.user);
          localStorage.setItem(LOCAL_STORAGE_KEY, jsonState);
        }
        this._appService.userResponse = ctx.user;
        return this._router.navigateByUrl('/app');
      },
    },
    guards: {
      isLoggedOut: () => {
        const storeCache = localStorage.getItem(LOCAL_STORAGE_KEY);
        return !storeCache || !JSON.parse(storeCache).token;
      },
    },
  };

  private _authMachine = Machine<AuthContext, AuthStateSchema, AuthEvent>(
    authMachineConfig
  ).withConfig(this.authMachineOptions);
  // @ts-ignore
  private _service = interpret(this._authMachine, { devTools: true }).start();
  // State<AuthContext, AuthEvent>
  authState$ = fromEventPattern<[any, EventObject]>(
    (handler) => {
      return this._service.onTransition(handler);
    },
    (_, service) => {
      return service.stop();
    }
  ).pipe(map(([state, _]) => state));

  /*  authState$ = from(this._service).pipe(
    takeUntil(this.isDone$),
    tap((state: any) => {
      console.log(state);
    })
  );*/

  send(event: any) {
    this._service.send(event);
  }

  start() {
    this._service.start();
  }
  constructor(
    private _router: Router,
    private _apollo: Apollo,
    private _appService: AppService
  ) {}

  // !TODO typescript error
  private _handleUserAuth(payload: UserAuth) {
    const { password, email, loginType } = payload;
    if (loginType === 'signup') {
      return this._apollo.mutate({
        mutation: gql`
          mutation createUser($appUser: AddAppUser!) {
            createUser(appUser: $appUser) {
              userName
              id
              token
            }
          }
        `,
        variables: {
          appUser: { password, userName: payload.username as string, email },
        },
      });
    }
    return this._apollo.subscribe({
      query: gql`
        query registeredUser($email: String!, $password: String!) {
          registeredUser(email: $email, password: $password) {
            userName
            id
            token
          }
        }
      `,
      variables: {
        email,
        password,
      },
    });
  }
}
