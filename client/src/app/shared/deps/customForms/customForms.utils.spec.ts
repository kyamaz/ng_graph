import {
  fieldsAreEqual,
  isObject,
  safeGet
} from 'src/app/shared/deps/customForms/customForms.utils';

describe('custom form  utils', () => {
  it('return false if arg is an array', () => {
    const MOCK = [1, 2];
    let result = isObject(MOCK);
    expect(result).toBe(false);
  });
  it('return false if arg is an string', () => {
    const MOCK = '';
    let result = isObject(MOCK);
    expect(result).toBe(false);
  });
  it('return true if arg is an object', () => {
    const MOCK = {};
    let result = isObject(MOCK);
    expect(result).toBe(true);
  });
  it('return null if path is invalid', () => {
    const MOCK = {
      dep_1: {
        dep_2: 'mock'
      }
    };
    let result = safeGet(MOCK, 'dep_1', 'dep_1');
    expect(result).toBeNull();
  });
  it('return prop value if path is valid', () => {
    const MOCK = {
      dep_1: {
        dep_2: 'mock'
      }
    };
    let result = safeGet(MOCK, 'dep_1', 'dep_2');
    expect(result).toEqual('mock');
  });

  it('return error args if fields are different', () => {
    const MOCK_FORM = {
      pwd: 'mock',
      pwdConfirm: 'mocky'
    };
    const MOCK_ERR: { [prop: string]: true } = { notEqual: true };
    let result = fieldsAreEqual(MOCK_FORM, ['pwd', 'pwdConfirm'], MOCK_ERR);
    expect(result).toEqual(MOCK_ERR);
  });
  it('return null if fields are identical', () => {
    const MOCK_FORM = {
      pwd: 'mock',
      pwdConfirm: 'mock'
    };
    const MOCK_ERR: { [prop: string]: true } = { notEqual: true };
    let result = fieldsAreEqual(MOCK_FORM, ['pwd', 'pwdConfirm'], MOCK_ERR);
    expect(result).toBeNull();
  });
});
