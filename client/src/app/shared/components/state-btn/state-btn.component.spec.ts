import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { StateBtnComponent } from './state-btn.component';
import { DebugElement } from '@angular/core';

const MOCK_SC = {
  /*   btn: {
    currentValue: 'red'
  }, */
  customStyle: {
    currentValue: 'height:15rem',
    previousValue: null,
    firstChange: false,
    isFirstChange: function() {
      return false;
    }
  }
};
describe('StateBtnComponent', () => {
  let component: StateBtnComponent;
  let fixture: ComponentFixture<StateBtnComponent>;
  let button: DebugElement;
  let span: DebugElement;
  let icon: DebugElement;
  let spinner: DebugElement;

  let warn = 'red';
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatButtonModule,
        CommonModule,
        MatProgressSpinnerModule,
        MatIconModule
      ],
      declarations: [StateBtnComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    warn = warn;
    fixture = TestBed.createComponent(StateBtnComponent);
    component = fixture.componentInstance;
    component.state = 'INITIAL';
    button = fixture.debugElement.query(By.css('button'));
    span = fixture.debugElement.query(By.css('span'));
    icon = fixture.debugElement.query(By.css('mat-icon'));
    spinner = fixture.debugElement.query(By.css('mat-spinner'));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should display initial message', fakeAsync(() => {
    span = fixture.debugElement.query(By.css('span'));

    fixture.detectChanges();
    tick();
    expect(span).toBeTruthy();

    expect(span.nativeElement.innerText).toBe('submit');
  }));

  it('should set success state on success', fakeAsync(() => {
    spyOn(component, 'ngOnChanges');

    component.state = 'SUCCESS';

    component.ngOnChanges(MOCK_SC);

    fixture.detectChanges();
    tick();
    fixture.whenStable().then(() => {
      expect(component.ngOnChanges).toHaveBeenCalled();

      fixture.detectChanges();
      expect(component.stateMap[component.state]).toEqual(
        jasmine.objectContaining({
          class: 'success',
          className: 'success',
          icon: 'done',
          isDone: true,
          isPending: false,
          txt: 'success'
        })
      );
    });

    expect(component.stateMap[component.state].isDone).toBeTruthy();
  }));
  it('should be pending state on pending', fakeAsync(() => {
    spyOn(component, 'ngOnChanges');

    component.state = 'PENDING';

    component.ngOnChanges(MOCK_SC);

    fixture.detectChanges();
    tick();
    fixture.whenStable().then(() => {
      expect(component.ngOnChanges).toHaveBeenCalled();

      fixture.detectChanges();
      expect(component.stateMap[component.state].isPending).toBeTruthy();
    });
  }));

  it('should be done state on errot', fakeAsync(() => {
    spyOn(component, 'ngOnChanges');

    component.state = 'ERROR';

    component.ngOnChanges(MOCK_SC);

    fixture.detectChanges();
    tick();
    fixture.whenStable().then(() => {
      expect(component.ngOnChanges).toHaveBeenCalled();

      fixture.detectChanges();
      expect(component.stateMap[component.state].isDone).toBeTruthy();
    });
  }));
});
