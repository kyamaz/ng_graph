import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputErrorComponent } from './input-error.component';

describe('InputErrorComponent', () => {
  let component: InputErrorComponent;
  let fixture: ComponentFixture<InputErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InputErrorComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputErrorComponent);
    component = fixture.componentInstance;
  });

  it('should create Input Err', () => {
    expect(component).toBeTruthy();
  });

  it('should set required error ', () => {
    const MOCK_SIMPLE_ERROR = { required: true };

    component.showError = true;
    component.errorType = MOCK_SIMPLE_ERROR;
    component._setErrMsg(MOCK_SIMPLE_ERROR);
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('p').innerText).toEqual(
      'mandatory field'
    );
  });
  it('should set required character length error', () => {
    const MOCK_LEN_ERROR = { minlength: { requiredLength: 8 } };
    const LEN_ERR_MSG = 'too short. Expected 8 characters';
    component.showError = true;
    component.errorType = MOCK_LEN_ERROR;
    component._setErrMsg(MOCK_LEN_ERROR);
    fixture.detectChanges();
    expect(component.len).toEqual('8 characters');

    expect(fixture.nativeElement.querySelector('p').innerText).toEqual(
      LEN_ERR_MSG
    );
  });
});
