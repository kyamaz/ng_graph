import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class AddAppUser {
  @Field()
  readonly userName: string;
  @Field()
  email: string;
  @Field()
  password: string;
}
