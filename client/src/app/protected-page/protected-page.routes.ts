import { DEFAULT_REDIRECT } from './../shared/routes/app-routing.module';
import { Routes } from '@angular/router';
import { PageComponent } from './container/page/page.component';
import { NotFoundComponent } from '@components/not-found/not-found.component';
import { RoutesGuardService } from '@routes/routes.guard.service';

export const ROUTES: Routes = [
  {
    path: '',
    component: PageComponent,
    canActivate: [RoutesGuardService],
    children: [
      {
        path: 'profile',
        loadChildren: () =>
          import('./../profile/profile.module').then((m) => m.ProfileModule),
      },
      {
        path: '',
        pathMatch: 'full',
        loadChildren: () =>
          import('./../home/home.module').then((m) => m.HomeModule),
      },
      {
        path: 'not-found',
        component: NotFoundComponent,
      },
    ],
  },
  { path: '**', redirectTo: DEFAULT_REDIRECT },
];
