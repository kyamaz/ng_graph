import { AppService } from './app.service';
import { MOCK_USER_SERVICE_RESPONSE } from './../fixtures/service';
import { API_ROOT } from './../const';
/* import { AppState } from './../store/app.state';
import { SetAppUSerAction } from './../store/app.actions';
 */
import { inject, TestBed } from '@angular/core/testing';

import { HttpInterceptService } from './http.intercept.service';
import { RouterTestingModule } from '@angular/router/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { MOCK_GENERIC_SERVICE_RESPONSE } from '../fixtures/service';
import { Router } from '@angular/router';

describe('HttpInterceptService', () => {
  let router: Router;
  let navigateSpy;
  let mockLocalStorage = null;
  let store = {};
  const protectedEndpoint = `${API_ROOT}api/account/`;

  beforeEach(() => {
    mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      },
      removeItem: (key: string) => {
        delete store[key];
      },
      clear: () => {
        store = {};
      }
    };

    spyOn(localStorage, 'getItem').and.callFake(mockLocalStorage.getItem);
    spyOn(localStorage, 'setItem').and.callFake(mockLocalStorage.setItem);
    spyOn(localStorage, 'removeItem').and.callFake(mockLocalStorage.removeItem);
    spyOn(localStorage, 'clear').and.callFake(mockLocalStorage.clear);
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [
        {
          provide: AppService,
          useValue: MOCK_GENERIC_SERVICE_RESPONSE
        },

        {
          provide: HTTP_INTERCEPTORS,
          useClass: HttpInterceptService,
          multi: true
        }
      ]
    });
    router = TestBed.get(Router);
    navigateSpy = spyOn(router, 'navigate');
  });
  afterEach(inject(
    [HttpTestingController],
    (httpMock: HttpTestingController) => {
      httpMock.verify();
    }
  ));
  it('should be create Intercept Service', () => {
    const service: HttpInterceptService = TestBed.get(HttpInterceptService);
    expect(service).toBeTruthy();
  });
  it('not add Authorization header on empty store', inject(
    [HttpClient, HttpTestingController],
    (http: HttpClient, httpMock: HttpTestingController) => {
      //make sure the service is called
      localStorage.setItem(
        'naister_user',
        JSON.stringify({
          access_token: null,
          user_name: 'null',
          user_id: null,
          is_admin: false,
          business_id: null,
          access_token_type: null,
          expires: null
        })
      );
      http.get(protectedEndpoint).subscribe(response => {
        console.log('resoi', response);
        expect(response).toBeTruthy();
      });

      const req = httpMock.expectOne(r => !r.headers.has('Authorization'));
      expect(req.request.headers.get('Authorization')).toBeFalsy();
      expect(req.request.method).toEqual('GET');

      httpMock.verify();
    }
  ));
  it('adds Authorization header on user Store', inject(
    [HttpClient, HttpTestingController],
    (http: HttpClient, httpMock: HttpTestingController) => {
      localStorage.setItem(
        'naister_user',
        JSON.stringify(MOCK_USER_SERVICE_RESPONSE.resp)
      );
      console.log('BEFFF', protectedEndpoint);

      http.get(protectedEndpoint).subscribe(response => {
        expect(response).toBeTruthy();
      });
      const req = httpMock.expectOne(r => r.headers.has('Authorization'));

      expect(req.request.headers.get('Authorization')).toEqual(
        `${MOCK_USER_SERVICE_RESPONSE.resp.access_token_type} ${MOCK_USER_SERVICE_RESPONSE.resp.access_token}`
      );
      expect(req.request.method).toEqual('GET');
      httpMock.verify();
    }
  ));
  it('access to page on  autorized response', inject(
    [HttpClient, HttpTestingController],
    (http: HttpClient, httpMock: HttpTestingController) => {
      //  store.dispatch(new SetAppUSerAction(MOCK_USER_STORE));

      localStorage.setItem(
        'naister_user',
        JSON.stringify(MOCK_USER_SERVICE_RESPONSE.resp)
      );

      router.navigate(['/']);
      expect(navigateSpy).toHaveBeenCalledWith(['/']);
    }
  ));

  /*  it('redirect to login on unautorized response', inject(
    [HttpClient, HttpTestingController],
    (http: HttpClient, httpMock: HttpTestingController) => {
      http.get(`${USER_API}detail`).subscribe(response => {
        expect(response).toBeTruthy();
      });
      const req = httpMock
        .expectOne(r => !r.headers.has('Authorization'))
        .flush(null, { status: 401, statusText: 'Unauthorized' });
      expect(navigateSpy).toHaveBeenCalledWith(['/', 'login']);

      httpMock.verify();
    }
  )); */
});
