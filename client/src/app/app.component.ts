import { ChangeDetectionStrategy, Component } from '@angular/core';
import { fadeInAnimation } from './shared/animations/animations';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [fadeInAnimation],
  changeDetection: ChangeDetectionStrategy.Default
})
export class AppComponent {
  constructor() {}

  getRouteAnimation(outlet: RouterOutlet): any {
    return outlet.activatedRouteData.animation;
  }
}
