import { FormGroup } from '@angular/forms';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { StateButtonType } from '@components/state-btn/state-btn.component';
import { safeGet } from '@forms/customForms.utils';
import { SimpleChanges } from '@angular/core';
import { FormsService } from '@forms/forms.services';
import { distinctUntilChanged, takeUntil, tap } from 'rxjs/operators';

export interface StateForm {
  setFormValidation(f: FormGroup): FormGroup;
}

export abstract class AbstractStateForm<T> implements StateForm {
  protected _destroy$: Subject<true> = new Subject();
  //template props
  public form: FormGroup;
  public btnState$: ReplaySubject<StateButtonType> = new ReplaySubject(1);
  public formValidState$: ReplaySubject<
    'VALID' | 'INVALID'
  > = new ReplaySubject(1);

  constructor(protected _formsService: FormsService<T>) {}

  useDestroy(): void {
    this._destroy$.next(true);
    this._destroy$.unsubscribe();
  }

  protected initStateForm(sc: SimpleChanges, propName: string) {
    if (sc[propName]) {
      const data = sc[propName].currentValue;
      if (!!data) {
        return this._formsService.build_form(data);
      }
    }
  }

  protected subFormChanges(
    form: FormGroup,
    initState: string
  ): Observable<'VALID' | 'INVALID'> {
    this.formValidState$.next(initState as 'VALID' | 'INVALID');
    return form.statusChanges.pipe(
      distinctUntilChanged(),
      tap(r => this.formValidState$.next(r)),
      takeUntil(this._destroy$)
    );
  }

  protected updateState(sc: SimpleChanges) {
    if (
      safeGet(sc, 'stateBtn', 'currentValue') !==
      safeGet(sc, 'stateBtn', 'previousValue')
    ) {
      const state: 'SUCCESS' | 'ERROR' | 'UPLOAD' = safeGet(
        sc,
        'stateBtn',
        'currentValue'
      );
      this.btnState$.next(state);
    }
  }

  abstract setFormValidation(form: FormGroup);
}
