import { filter, map, takeUntil, tap } from 'rxjs/operators';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {
  Authenticating,
  AuthMachineService,
} from '@login/services/auth.machine.service';
import { LoginType } from '@login/login/login.component';

enum _ERROR_MSG {
  required = 'mandatory field',
  minlength = 'too short. Expected ',
  email = 'invalid email',
  mismatch = "field doesn't match",
  same = "doesn't match",
}

/*
error message handling helpers
 */
function setErrorMessage(error: ValidationErrors) {
  if (error) {
    const errKey = Object.keys(error)[0];
    if (errKey === 'minlength') {
      return `field should contain ${error.minlength.requiredLength}. Missing ${
        error.minlength.requiredLength - error.minlength.actualLength
      } characters`;
    }
    return errKey && _ERROR_MSG[errKey] ? _ERROR_MSG[errKey] : 'invalid field';
  }
}

function isErrorVisible(fc: FormControl): boolean {
  return fc.touched && fc.invalid;
}

@Component({
  selector: 'signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SigninComponent implements OnInit, OnChanges, OnDestroy {
  @Input() loginType: LoginType;

  public form: FormGroup;
  public formState$: Observable<any>;
  public isSignup$: Subject<boolean> = new Subject<boolean>();
  private _confirmError = {
    same: true,
  };
  protected _destroy$: Subject<true> = new Subject<true>();
  private _confirmedPWd = '';
  private _username = '';

  public setErrorMessage = setErrorMessage.bind(this);
  public isErrorVisible = isErrorVisible.bind(this);

  constructor(
    private _fb: FormBuilder,
    private _authMachineService: AuthMachineService
  ) {}

  ngOnDestroy() {
    this._destroy$.next(true);
    this._destroy$.unsubscribe();
  }

  ngOnChanges(sc: SimpleChanges) {
    this.isSignup$.next(sc.loginType.currentValue === 'signup');
  }

  ngOnInit() {
    this.form = this._setForm();
    this._formTypeToggle$().pipe(takeUntil(this._destroy$)).subscribe();
    this._updateValidationOnPwdUpdate()
      .pipe(takeUntil(this._destroy$))
      .subscribe();
    this.formState$ = this._authMachineService.authState$.pipe(
      map((state) => state.value),
      takeUntil(this._destroy$)
    );
  }

  private _formTypeToggle$(): Observable<boolean> {
    return this.isSignup$.pipe(
      tap((isSignup) => this._updateConfirmCtrl(isSignup))
    );
  }

  private _setForm(): FormGroup {
    return this._fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: [
        '',
        Validators.compose([Validators.required, Validators.minLength(8)]),
      ],
    });
  }

  private _updateValidationOnPwdUpdate(): Observable<string> {
    return this.form.get('password').valueChanges.pipe(
      filter((value) => !!this.form.get('confirm')),
      tap((pwd) => {
        this._areEquals(['password'], this.form.get('confirm') as any);
        this.form.get('confirm').updateValueAndValidity();
      })
    );
  }

  private _updateConfirmCtrl(isSignup: boolean): void {
    if (isSignup) {
      // username
      this.form.addControl('username', new FormControl(this._username));
      this.form.get('username').setValidators(Validators.required);
      this.form.get('username').updateValueAndValidity();
      this.form
        .get('username')
        .valueChanges.pipe(
          tap((user) => (this._username = user)),
          takeUntil(this._destroy$)
        )
        .subscribe();
      // confirm
      this.form.addControl('confirm', new FormControl(this._confirmedPWd));
      this.form
        .get('confirm')
        .setValidators([this._areEquals.bind(this, ['password'])]);
      this.form.get('confirm').updateValueAndValidity();
    } else {
      this.form.removeControl('confirm');
      this.form.removeControl('username');
    }
  }

  public onSubmit(login = this.form.value): void {
    this._authMachineService.send(
      new Authenticating({ ...login, loginType: this.loginType })
    );
  }

  private _areEquals(
    targetPath: Array<string>,
    srcFc: FormControl
  ): ValidationErrors | null {
    if (srcFc) {
      const targetValue = this.form.get(targetPath).value;
      if (
        targetValue !== srcFc.value ||
        srcFc.value.length === 0 ||
        targetValue === null // invalid target path
      ) {
        this._confirmedPWd = srcFc.value;
        return this._confirmError;
      }
      this._confirmedPWd = srcFc.value;

      return null;
    }
    return null;
  }
}
