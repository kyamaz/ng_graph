import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormError } from 'src/app/shared/deps/customForms/forms.error';

describe('error form state matcher', () => {
  it('should return true if control is invalid && dirty', () => {
    const MOCK = new FormError();
    const MOCK_FORM = new FormGroup({
      mock: new FormControl(null, Validators.required)
    });

    MOCK_FORM.get('mock').markAsDirty();
    MOCK_FORM.get('mock').markAsTouched();
    const res = MOCK.isErrorState(
      MOCK_FORM.get('mock') as any,
      MOCK_FORM as any
    );
    expect(res).toBe(true);
  });

  it('should return false if control is invalid && untouched', () => {
    const MOCK = new FormError();
    const MOCK_FORM = new FormGroup({
      mock: new FormControl(null, Validators.required)
    });
    const res = MOCK.isErrorState(
      MOCK_FORM.get('mock') as any,
      MOCK_FORM as any
    );
    expect(res).toBe(false);
  });
  it('should return false if control is valid', () => {
    const MOCK = new FormError();
    const MOCK_FORM = new FormGroup({
      mock: new FormControl('test', Validators.required)
    });
    MOCK_FORM.get('mock').markAsDirty;

    const res = MOCK.isErrorState(
      MOCK_FORM.get('mock') as any,
      MOCK_FORM as any
    );
    expect(res).toBe(false);
  });
});
