import { RouterModule } from '@angular/router';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './containers/main/main.component';
import { ROUTES } from './home.routes';
import { PersonListComponent } from './components/person-list/person-list.component';
import { PersonItemComponent } from './components/person-item/person-item.component';
import { RowComponent } from './components/row/row.component';
import { AutoCompleteComponent } from './components/auto-complete/auto-complete.component';
import { SortByComponent } from './components/sort-by/sort-by.component';
import { SortListComponent } from './components/sort-list/sort-list.component';
import { ListActionsComponent } from './components/list-actions/list-actions.component';

@NgModule({
  declarations: [
    MainComponent,
    PersonListComponent,
    PersonItemComponent,
    RowComponent,
    AutoCompleteComponent,
    SortByComponent,
    SortListComponent,
    ListActionsComponent,
  ],
  imports: [CommonModule, SharedModule, RouterModule.forChild(ROUTES)],
})
export class HomeModule {}
