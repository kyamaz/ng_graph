import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class UpdateAppUser {
  @Field({ nullable: true })
  readonly userName: string;
  @Field({ nullable: true })
  readonly email: string;
  @Field({ nullable: true })
  password: string;
  @Field({ nullable: true })
  firstName: string;
  @Field({ nullable: true })
  lastName: string;
  @Field({ nullable: true })
  avatar: string;
}
