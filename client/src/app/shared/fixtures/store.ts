export const MOCK_USER_STORE = {
  access_token: 'false',
  access_token_type: 'fake_type',
  user_name: 'false',
  user_id: 0,
  is_admin: false,
  business_id: 0,
  expires: null
};
export const MOCK_INIT_USER_STORE = {
  access_token: null,
  access_token_type: null,
  user_name: null,
  user_id: 0,
  is_admin: false,
  business_id: 0
};
export const MOCK_INIT_ADMIN_STORE = {
  ...MOCK_USER_STORE,

  is_admin: true
};
