import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'list-actions',
  templateUrl: './list-actions.component.html',
  styleUrls: ['./list-actions.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListActionsComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
