import {
  Args,
  Parent,
  Query,
  ResolveField,
  ResolveProperty,
  Resolver,
} from '@nestjs/graphql';
import { LinkedInService } from './../linked-in.service';
import { CompanyType } from './company.dto';
import { Company } from './company.interface';
import { Person } from '../person/person.interface';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from '../../auth.guard';

@Resolver((of) => CompanyType)
export class CompanyResolver {
  constructor(private _linkedInService: LinkedInService) {}

  @Query(() => [CompanyType])
  @UseGuards(AuthGuard)
  async companies(): Promise<Company[]> {
    return await this._linkedInService.getCompanyList();
  }
  @Query(() => CompanyType)
  @UseGuards(AuthGuard)
  async company(@Args('id') id: string): Promise<Company> {
    return await this._linkedInService.getCompany(id);
  }
  // resolve fields
  @ResolveProperty()
  async people(@Parent() company: Company): Promise<Person[]> {
    return this._linkedInService.getCompanyPerson(company.personIds);
  }
}
