
FROM node:10.16-alpine 


# set docker working directory
RUN mkdir -p /usr/src/app  
WORKDIR /usr/src/app


COPY package.json package.lock ./
RUN npm install
COPY . /usr/src/app
RUN yarn build

# Copy the main application.

COPY client/src/app/dist  /usr/share/nginx/html
# command
CMD ["nginx", "-g", "daemon off;"]



