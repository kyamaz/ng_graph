import { Injectable } from '@angular/core';
import {
  assign,
  EventObject,
  interpret,
  Machine,
  MachineOptions,
} from 'xstate';
import { fromEventPattern } from 'rxjs';
import { map } from 'rxjs/operators';

export class InitState {
  readonly type = 'INIT';

  constructor() {}
}

export class UpdateState {
  readonly type = 'TRANSLATE';

  constructor() {}
}

export class DefaultState {
  readonly type = 'DEFAULT';

  constructor() {}
}

const signinMachineConfig = {
  id: 'signinMachine',
  initial: 'pristine',
  context: {
    dirty: false,
    isTranslated: false,
  },
  states: {
    pristine: {
      on: {
        INIT: [{ target: 'initial', actions: ['markDirty'] }],
      },
    },
    translate: {
      on: {
        DEFAULT: [{ target: 'initial', actions: ['resetTranslation'] }],
      },
    },
    initial: {
      on: {
        TRANSLATE: [{ target: 'translate', actions: ['setTranslation'] }],
      },
    },
  },
};

@Injectable({
  providedIn: 'root',
})
export class SigninMachineService {
  authMachineOptions: Partial<MachineOptions<any, any>> = {
    actions: {
      resetTranslation: assign<any, any>((_, event) => {
        return {
          isTranslated: false,
        };
      }),
      setTranslation: assign<any, any>((_, event) => {
        return {
          isTranslated: true,
        };
      }),
      markDirty: assign<any, any>((_, event) => {
        return {
          dirty: true,
        };
      }),
    },
  };

  //  apply  config
  private _signinMachine = Machine<any, any, any>(
    signinMachineConfig
  ).withConfig(this.authMachineOptions);

  private service = interpret(this._signinMachine, { devTools: true }).start();
  authState$ = fromEventPattern<[any, EventObject]>(
    (handler) => {
      return this.service.onTransition(handler);
    },
    (_, service) => service.stop()
  ).pipe(
    map(([state, _]) => {
      return state;
    })
  );

  send(event: any) {
    this.service.send(event);
  }

  start() {
    this.service.start();
  }

  constructor() {}
}
