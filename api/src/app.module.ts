import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { MongooseModule } from '@nestjs/mongoose';
import { GraphqlOptions } from './graphql.option';
import { AppUserModule } from './app-user/app-user.module';
import { LinkedInModule } from './linked-in/linked-in.module';

@Module({
  imports: [
    GraphQLModule.forRootAsync({ useClass: GraphqlOptions }),
    //  MongooseModule.forRoot('mongodb://localhost/nestgraphql'),
    MongooseModule.forRoot('mongodb://mongo:27017/nestgraphql'),
    AppUserModule,
    LinkedInModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
