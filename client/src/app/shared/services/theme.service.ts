import { Injectable } from '@angular/core';

enum darkTheme {
  'primary-color' = '#ce3cba',
  'background-color' = '#2f2f2f',
  'text-color' = '#fffafa',
  'text-primary' = '#fffafa',
}

enum lightTheme {
  'primary-color' = '#ce3cba',
  'background-color' = '#f9f9f9',
  'text-color' = '#5e6c84',
  'text-primary' = '#aa2b99',
}

@Injectable({
  providedIn: 'root',
})
export class ThemeService {
  constructor() {}

  toggleTheme(theme: 'light' | 'dark'): void {
    const _t = theme === 'light' ? lightTheme : darkTheme;
    this.setTheme(_t);
  }

  private setTheme(theme: {}) {
    Object.keys(theme).forEach((k) =>
      document.documentElement.style.setProperty(`--${k}`, theme[k])
    );
  }
}
