import { MainComponent } from './containers/main/main.component';
import { Routes } from '@angular/router';

export const ROUTES: Routes = [
  {
    path: '',
    component: MainComponent,
  },
];
