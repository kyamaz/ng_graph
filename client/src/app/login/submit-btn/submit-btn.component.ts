import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { SubmitContentDefaultComponent } from '@login/submit-content-default/submit-content-default.component';
import { tap } from 'rxjs/operators';
import { SubmitContentSuccessComponent } from '@login/submit-content-success/submit-content-success.component';
import { SubmitContentLoadingComponent } from '@login/submit-content-loading/submit-content-loading.component';
import { SubmitContentErrorComponent } from '@login/submit-content-error/submit-content-error.component';

const btnContent = {
  unauthorized: SubmitContentDefaultComponent,
  authorizing: SubmitContentLoadingComponent,
  authorized: SubmitContentSuccessComponent,
  authorizingFailed: SubmitContentErrorComponent,
};

@Component({
  selector: 'submit-btn',
  templateUrl: './submit-btn.component.html',
  styleUrls: ['./submit-btn.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubmitBtnComponent implements OnChanges, OnInit, AfterViewInit {
  @Input() isValid: boolean;

  @Input() state: string;
  @ViewChild('content', { read: ViewContainerRef })
  public contentRef: ViewContainerRef;

  public btnState$: ReplaySubject<string> = new ReplaySubject(1);

  constructor(
    protected cfr: ComponentFactoryResolver,
    protected _changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnChanges(change: SimpleChanges) {
    if (change.state) {
      this.btnState$.next(change.state.currentValue);
    }
  }

  ngOnInit() {}

  ngAfterViewInit() {
    this.btnState$
      .pipe(
        tap((state) => {
          this.makeComp(state);
        })
      )
      .subscribe();
  }

  makeComp(state): void {
    if (!state) {
      console.warn('illegale submit state :', state);
    }
    const comp = btnContent.hasOwnProperty(state)
      ? btnContent[state]
      : SubmitContentDefaultComponent;
    const factory: any = this.cfr.resolveComponentFactory(comp);

    this.contentRef.clear();
    this.contentRef.createComponent(factory);

    // (<any>componentRef.instance).viewData = comp.data;
    // (<any>componentRef.instance).source = comp.source;
  }
}
