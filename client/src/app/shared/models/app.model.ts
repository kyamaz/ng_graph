export interface AppPerson {
  _typename: string;
  id: string;
  firstName: string;
  lastName: string;
  avatar: string;
  jobTitle: string;
  jobDescription: string;
  phoneNumber: string;
  company: Partial<Company>;
}

export interface Company {
  _typename: string;
  id: string;
  name: string;
  logo: string;
  phoneNumber: string;
  people: AppPerson[];
}
