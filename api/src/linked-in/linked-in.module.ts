import { Module } from '@nestjs/common';
import { LinkedInService } from './linked-in.service';
import { MongooseModule } from '@nestjs/mongoose';
import { PersonSchema } from './person/person.schema';
import { CompanySchema } from './company/company.schema';
import { PersonResolver } from './person/person.resolver';
import { CompanyResolver } from './company/company.resolver';
import { AppUserService } from '../app-user/app-user.service';
import { AppUserResolver } from '../app-user/app-user.resolver';
import { AppUserSchema } from '../app-user/app-user.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Person', schema: PersonSchema },
      { name: 'Company', schema: CompanySchema },
      { name: 'AppUser', schema: AppUserSchema },
    ]),
  ],
  providers: [AppUserService, LinkedInService, PersonResolver, CompanyResolver],
})
export class LinkedInModule {}
