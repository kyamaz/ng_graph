import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { UserGraph } from '@models/user.model';
import { FormsService } from '@forms/forms.services';
import { FormGroup, Validators } from '@angular/forms';
import { takeUntil, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AppService } from '@appServices/app.service';

type EditProfile = Pick<
  UserGraph,
  'userName' | 'firstName' | 'lastName' | 'avatar'
>;

@Component({
  selector: 'edit-profile-form',
  templateUrl: './edit-profile-form.component.html',
  styleUrls: ['./edit-profile-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditProfileFormComponent implements OnInit, OnChanges, OnDestroy {
  @Input() user: EditProfile;

  form: FormGroup;
  isImgValid$: Subject<boolean> = new Subject<boolean>();
  private _destroyed$: Subject<true> = new Subject<true>();

  constructor(
    public formService: FormsService<EditProfile>,
    private _appService: AppService
  ) {}

  ngOnChanges(sc: SimpleChanges) {
    if (!sc.user.firstChange) {
      this.form.setValue(sc.user.currentValue);
    }
  }

  ngOnInit(): void {
    this.form = this.formService.build_form(this.user);
    this.form = this.formService.setCtrlsValidation(this.form, [
      {
        validations: {
          validator: [Validators.required],
        },
        path: ['userName'],
      },
      {
        validations: {
          custom: [{ name: 'ValidatorIsUrl', arg: [true] }],
        },
        path: ['avatar'],
      },
    ]);

    this.form
      .get('avatar')
      .statusChanges.pipe(
        tap((status) => this.isImgValid$.next(status === 'VALID')),
        takeUntil(this._destroyed$)
      )
      .subscribe();
  }
  ngOnDestroy(): void {
    this._destroyed$.next(true);
  }

  onSubmit() {
    this._appService
      .updateUser(this.form.value)
      .pipe(takeUntil(this._destroyed$))
      .subscribe();
  }
}
