import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { SigninComponent } from './signin/signin.component';
import { InputErrorComponent } from './input-error/input-error.component';
import { SubmitBtnComponent } from './submit-btn/submit-btn.component';
import { SubmitContentDefaultComponent } from './submit-content-default/submit-content-default.component';
import { SubmitContentLoadingComponent } from './submit-content-loading/submit-content-loading.component';
import { SubmitContentSuccessComponent } from './submit-content-success/submit-content-success.component';
import { SubmitContentErrorComponent } from './submit-content-error/submit-content-error.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { ROUTES } from './login.routes';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatIconModule,
    MatProgressSpinnerModule,
    RouterModule.forChild(ROUTES),
  ],
  declarations: [
    LoginComponent,
    SigninComponent,
    InputErrorComponent,
    SubmitBtnComponent,
    SubmitContentDefaultComponent,
    SubmitContentLoadingComponent,
    SubmitContentSuccessComponent,
    SubmitContentErrorComponent,
  ],
  providers: [],
})
export class LoginModule {}
