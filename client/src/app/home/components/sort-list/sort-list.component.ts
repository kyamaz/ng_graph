import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import {
  DataMachineService,
  FetchMore,
  SortBy,
} from '@appServices/data-machine.service';
import { AppPerson } from '@models/app.model';
import { map, takeUntil, tap } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { SortKey, SortOrder, SortState } from '@models/index';
import { AppService } from '@appServices/app.service';
import { PopoverService } from '@appServices/popover.service';

@Component({
  selector: 'sort-list',
  templateUrl: './sort-list.component.html',
  styleUrls: ['./sort-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SortListComponent implements OnInit, OnDestroy {
  public sort$: Observable<SortState>;
  private _destroy$: Subject<true> = new Subject();

  constructor(
    private _dataMachineService: DataMachineService<
      Omit<AppPerson, 'phoneNumber'>
    >,
    private _appService: AppService,
    private _popoverService: PopoverService
  ) {}

  ngOnDestroy() {
    this._destroy$.next(true);
  }
  ngOnInit() {
    this.sort$ = this._appService.context$.pipe(takeUntil(this._destroy$));
  }

  sortBy(
    key: '_id' | 'lastName' | 'jobTitle',
    sort: { key: SortKey; order: SortOrder }
  ): void {
    const order = sort.order === 'asc' ? 'desc' : 'asc';
    this._dataMachineService.send(new SortBy({ key, order }));
    this._popoverService.close();
  }
}
