import { FormArray, FormControl, FormGroup } from '@angular/forms';
import {
  FormsService,
  updateFormArray
} from 'src/app/shared/deps/customForms/forms.services';
import { formArrayAction } from './forms.models';

describe('form service', () => {
  let MOCK_FORM_ARR: FormArray;
  let service: FormsService<any>;
  let MOCK_FORM: FormGroup;
  let MOCK_FORM_NESTED: FormGroup;

  beforeEach(() => {
    service = new FormsService();
    MOCK_FORM_ARR = new FormArray([
      new FormControl(1),
      new FormControl(2),
      new FormControl(3)
    ]);
    MOCK_FORM = new FormGroup({
      dep_1: new FormGroup({ dep_2: new FormControl('fake') })
    });
    MOCK_FORM_NESTED = new FormGroup({
      dep_1: new FormGroup({ dep_2: MOCK_FORM_ARR })
    });
    spyOn(console, 'error');
  });

  it('should add item to form array', () => {
    const MOCK_ACTION: formArrayAction = { type: 'push', id: 4 };
    expect(MOCK_FORM_ARR.value.length).toEqual(3);

    updateFormArray(MOCK_FORM_ARR, MOCK_ACTION);
    expect(MOCK_FORM_ARR.value.length).toEqual(4);
  });
  it('should remove item to form array', () => {
    const MOCK_ACTION: formArrayAction = { type: 'remove', id: 0 };

    expect(MOCK_FORM_ARR.value.length).toEqual(3);
    let res = updateFormArray(MOCK_FORM_ARR, MOCK_ACTION);
    expect(res.value.length).toEqual(2);
  });

  it('should return an form group from string', () => {
    const MOCK = 'test';
    let f = service.build_form(MOCK);
    expect(f instanceof FormGroup).toBeDefined();
    expect(f.value).toEqual({ data: MOCK });
  });
  it('should return an form group from array', () => {
    const MOCK = [1, 2, 3];
    let f = service.build_form(MOCK);
    expect(f instanceof FormGroup).toBeDefined();
    expect(f.value).toEqual({ data: MOCK });
  });
  it('should return an form group from object', () => {
    const MOCK = { mock: true };
    let f = service.build_form(MOCK);
    expect(f instanceof FormGroup).toBeDefined();
    expect(f.value).toEqual(MOCK);
  });

  it('should not build a form', () => {
    const MOCK = new FormControl('test');
    let f = service._rec_build_form(MOCK);
    expect(MOCK).toEqual(MOCK);
  });
  it('should  build a form', () => {
    const MOCK = 'test';
    let f = service._rec_build_form(MOCK);
    const EXPECTED_MOCK = new FormControl(MOCK);
    expect(EXPECTED_MOCK instanceof FormControl).toBeTruthy();
    expect(EXPECTED_MOCK.value).toEqual(MOCK);
  });

  it('should return form child given path', () => {
    const res = service.getSubFormByList(MOCK_FORM, ['dep_1', 'dep_2']);
    expect(res instanceof FormControl).toBeTruthy();
    expect(res.value).toEqual('fake');
  });

  it('should console error on invalid root path ', () => {
    service.getSubFormByList(MOCK_FORM, ['fail']);
    expect(console.error).toHaveBeenCalled();
  });
  it('should console error on invalid sub  path ', () => {
    service.getSubFormByList(MOCK_FORM, ['dep_1', 'fail']);
    expect(console.error).toHaveBeenCalled();
  });

  it('should get value given path', () => {
    const res = service.getSubFormValue(MOCK_FORM, 'dep_1', 'dep_2');
    expect(res).toEqual('fake');
  });

  it('should get truthy pristine state given path', () => {
    const res = service.isSubFormPristine(MOCK_FORM, 'dep_1', 'dep_2');
    expect(res).toBe(true);
  });
  it('should get false pristine state given path', () => {
    MOCK_FORM.get(['dep_1', 'dep_2']).markAsDirty();
    const res = service.isSubFormPristine(MOCK_FORM, 'dep_1', 'dep_2');
    expect(res).toBe(false);
  });

  it('should get subform given path in format { prop:subform}', () => {
    const res = service.getSubFormControls(MOCK_FORM, 'dep_1');
    const MOCK_RES: any = {
      dep_2: new FormControl('fake')
    };
    expect(res['dep_2'] instanceof FormControl).toBeTruthy();
    expect(res['dep_2'].value).toEqual(MOCK_RES.dep_2.value);
  });
  it('should get list subform given path in format list subform', () => {
    const res = service.getSubFormArrayControls(
      MOCK_FORM_NESTED,
      'dep_1',
      'dep_2'
    );
    expect(res instanceof Array).toBeTruthy();
    expect(res.length).toEqual(3);
    expect(res[0] instanceof FormControl).toBeTruthy();
  });
  it('should log error on get invalid subborm', () => {
    const res = service.getSubFormArrayControls(MOCK_FORM_NESTED, 'dep_1');
    expect(console.error).toHaveBeenCalled();
  });

  it('should set subform value', () => {
    const MOCK_ARG = 'test';
    expect(MOCK_FORM.get(['dep_1', 'dep_2']).value).not.toEqual(MOCK_ARG);
    service.setSubFormValue(MOCK_FORM, MOCK_ARG, 'dep_1', 'dep_2');
    expect(MOCK_FORM.get(['dep_1', 'dep_2']).value).toEqual(MOCK_ARG);
  });

  it('should set subform given path', () => {
    const MOCK_ARG = { fake: 'new' };

    expect(MOCK_FORM.get(['dep_1', 'dep_2'])).toBeTruthy();
    service.setSubFormControl(MOCK_FORM, MOCK_ARG, 'dep_1');
    expect(MOCK_FORM.get(['dep_1', 'dep_2'])).toBeFalsy();
    expect(MOCK_FORM.get(['dep_1', 'fake'])).toBeTruthy();
  });
  /*
  it('should log error set subform array for given path', () => {
    const MOCK_ARG = { fake: 'new' };

    expect(MOCK_FORM_NESTED.get(['dep_1', 'dep_2'])).toBeTruthy();
    service.setSubFormControl(MOCK_FORM_NESTED, MOCK_ARG, 'dep_1');
    console.log(MOCK_FORM_NESTED);

    expect(console.error).toHaveBeenCalled();
  }); */
  it('should remove subform given path', () => {
    expect(MOCK_FORM.get(['dep_1', 'dep_2'])).toBeTruthy();
    service.deleteSubForm(MOCK_FORM, 'dep_1', 'dep_2');
    expect(MOCK_FORM.get(['dep_1', 'dep_2'])).toBeFalsy();
  });
  it('should push subform to form array given path', () => {
    const MOCK_ARG = 'new';
    expect(MOCK_FORM_NESTED.get(['dep_1', 'dep_2']).value.length).toEqual(3);
    service.pushSubFormArray(MOCK_FORM_NESTED, MOCK_ARG, 'dep_1', 'dep_2');
    expect(MOCK_FORM_NESTED.get(['dep_1', 'dep_2']).value.length).toEqual(4);
    expect(MOCK_FORM_NESTED.get(['dep_1', 'dep_2', '3']).value).toBe(MOCK_ARG);
  });

  it('should delete subform to form array given path', () => {
    expect(MOCK_FORM_NESTED.get(['dep_1', 'dep_2']).value.length).toEqual(3);
    service.deleteSubFormArray(MOCK_FORM_NESTED, 2, 'dep_1', 'dep_2');
    expect(MOCK_FORM_NESTED.get(['dep_1', 'dep_2']).value.length).toEqual(2);
  });

  it('should get form array given path', () => {
    expect(MOCK_FORM_NESTED.get(['dep_1', 'dep_2']).value.length).toEqual(3);
    const res = service.getSubFormArrays(MOCK_FORM_NESTED, 'dep_1', 'dep_2');
    expect(res instanceof FormArray).toBeTruthy();
  });

  it('should return form with set subform value', () => {
    const MOCK_ARG = 'test';
    expect(MOCK_FORM.get(['dep_1', 'dep_2']).value).not.toEqual(MOCK_ARG);
    const res = service.setCtrlValue(MOCK_FORM, MOCK_ARG, 'dep_1', 'dep_2');
    expect(res.get(['dep_1', 'dep_2']).value).toEqual(MOCK_ARG);
  });

  it('should return form with set subform value', () => {
    const MOCK_ARG = [
      {
        value: 66,
        path: ['dep_1', 'dep_2', '0']
      },
      {
        value: 666,
        path: ['dep_1', 'dep_2', '1']
      }
    ];
    expect(MOCK_FORM_NESTED.get(['dep_1', 'dep_2', '0']).value).toEqual(1);
    expect(MOCK_FORM_NESTED.get(['dep_1', 'dep_2', '1']).value).toEqual(2);

    const res = service.setCtrlsValues(MOCK_FORM_NESTED, MOCK_ARG);
    expect(res.get(['dep_1', 'dep_2', '0']).value).toEqual(66);
    expect(res.get(['dep_1', 'dep_2', '1']).value).toEqual(666);
  });

  //TODO deleteSubArray

  it('should return form with buildin validation', () => {
    expect(MOCK_FORM.get(['dep_1', 'dep_2']).validator).toBeNull();

    const res = service.setCtrlValidations(
      MOCK_FORM,
      { validator: [{ name: 'required' }] },
      'dep_1',
      'dep_2'
    );

    expect(res.get(['dep_1', 'dep_2']).validator).not.toBeNull();
    res.get(['dep_1', 'dep_2']).setValue(null);
    expect(res.get(['dep_1', 'dep_2']).errors.required).toBeTruthy();
  });

  it('should return form with custom validation', () => {
    expect(MOCK_FORM.get(['dep_1', 'dep_2']).validator).toBeNull();

    const res = service.setCtrlValidations(
      MOCK_FORM,
      { custom: [{ name: 'validatorTxtMinLen', arg: [5] }] },
      'dep_1',
      'dep_2'
    );

    expect(res.get(['dep_1', 'dep_2']).validator).not.toBeNull();
    res.get(['dep_1', 'dep_2']).setValue('1');
    expect(res.get(['dep_1', 'dep_2']).errors.invalidLength).toBeTruthy();
  });

  it('should return form with custom validation on list on subfom', () => {
    expect(MOCK_FORM_NESTED.get(['dep_1', 'dep_2', '0']).validator).toBeNull();
    expect(MOCK_FORM_NESTED.get(['dep_1', 'dep_2', '1']).validator).toBeNull();

    const res = service.setCtrlsValidation(MOCK_FORM_NESTED, [
      {
        validations: { validator: [{ name: 'required' }] },
        path: ['dep_1', 'dep_2', '0']
      },
      {
        validations: { validator: [{ name: 'required' }] },
        path: ['dep_1', 'dep_2', '1']
      }
    ]);
    expect(res.get(['dep_1', 'dep_2', '0']).validator).not.toBeNull();
    expect(res.get(['dep_1', 'dep_2', '1']).validator).not.toBeNull();

    res.get(['dep_1', 'dep_2', '0']).setValue(null);
    expect(res.get(['dep_1', 'dep_2', '0']).errors.required).toBeTruthy();
  });

  it('should add   required validation on list on subfom', () => {
    expect(MOCK_FORM_NESTED.get(['dep_1', 'dep_2', '1']).validator).toBeNull();

    const res = service.setCtrlsAreRequired(MOCK_FORM_NESTED, [
      {
        validations: { custom: [{ name: 'validatorTxtMinLen', arg: [2] }] },
        path: ['dep_1', 'dep_2', '0']
      },
      {
        validations: { custom: [{ name: 'validatorTxtMinLen', arg: [2] }] },
        path: ['dep_1', 'dep_2', '1']
      }
    ]);
    expect(res.get(['dep_1', 'dep_2', '1']).validator).not.toBeNull();

    res.get(['dep_1', 'dep_2', '1']).setValue(null);
    expect(res.get(['dep_1', 'dep_2', '1']).errors.required).toBeTruthy();
  });

  it('should return form with cleared validation', () => {
    expect(MOCK_FORM.get(['dep_1', 'dep_2']).validator).toBeNull();

    const res = service.setCtrlValidations(
      MOCK_FORM,
      { validator: [{ name: 'required' }] },
      'dep_1',
      'dep_2'
    );

    expect(res.get(['dep_1', 'dep_2']).validator).not.toBeNull();

    service.clearValidations(res, [
      {
        path: ['dep_1', 'dep_2']
      }
    ]);
    expect(res.get(['dep_1', 'dep_2']).validator).toBeNull();
  });

  it('should return invalid url message', () => {
    const res = service.setCtrlValidations(
      MOCK_FORM,
      { custom: [{ name: 'ValidatorIsUrl' }] },
      'dep_1',
      'dep_2'
    );
    res.get(['dep_1', 'dep_2']).setValue('test');
    expect(res.get(['dep_1', 'dep_2']).errors).toEqual({
      notUrl: {
        msg: 'PLEASE_CHECK_YOUR_URL'
      }
    });
  });
  /*   it('should return valid url message', () => {
    const res = service.setCtrlValidations(MOCK_FORM, { custom: [{ name: 'ValidatorIsUrl' }] }, 'dep_1', 'dep_2');
    res.get(['dep_1', 'dep_2']).setValue('http://wwww.valid.com');

    console.log(res.get(['dep_1', 'dep_2']));
    expect(res.get(['dep_1', 'dep_2']).errors).toBeNull();
  }); */

  it('should return invalidate positive integer ', () => {
    const res = service.setCtrlValidations(
      MOCK_FORM,
      { custom: [{ name: 'ValidatorInvalidInteger' }] },
      'dep_1',
      'dep_2'
    );
    res.get(['dep_1', 'dep_2']).setValue(-12);
    expect(res.get(['dep_1', 'dep_2']).errors).toEqual({
      invalid_number: {
        msg: 'INVALID_NUMBER'
      }
    });
  });
  it('should return validate positive integer', () => {
    const res = service.setCtrlValidations(
      MOCK_FORM,
      { custom: [{ name: 'ValidatorInvalidInteger' }] },
      'dep_1',
      'dep_2'
    );
    res.get(['dep_1', 'dep_2']).setValue(1);
    expect(res.get(['dep_1', 'dep_2']).errors).toBeNull();
  });

  it('should return validate max num', () => {
    const res = service.setCtrlValidations(
      MOCK_FORM,
      { custom: [{ name: 'ValidatorMaxNumber', arg: [5] }] },
      'dep_1',
      'dep_2'
    );
    res.get(['dep_1', 'dep_2']).setValue(1);
    expect(res.get(['dep_1', 'dep_2']).errors).toBeNull();
  });

  it('should return invalidate max num ', () => {
    const res = service.setCtrlValidations(
      MOCK_FORM,
      { custom: [{ name: 'ValidatorMaxNumber', arg: [5] }] },
      'dep_1',
      'dep_2'
    );
    res.get(['dep_1', 'dep_2']).setValue(12);
    expect(res.get(['dep_1', 'dep_2']).errors).toEqual({
      invalid_max_value: {
        msg: 'INVALID_MAX_NUMBER'
      }
    });
  });

  it('should return validate min num', () => {
    const res = service.setCtrlValidations(
      MOCK_FORM,
      { custom: [{ name: 'ValidatorMinNumber', arg: [5] }] },
      'dep_1',
      'dep_2'
    );
    res.get(['dep_1', 'dep_2']).setValue(7);
    expect(res.get(['dep_1', 'dep_2']).errors).toBeNull();
  });
  it('should return invalidate min num', () => {
    const res = service.setCtrlValidations(
      MOCK_FORM,
      { custom: [{ name: 'ValidatorMinNumber', arg: [5] }] },
      'dep_1',
      'dep_2'
    );
    res.get(['dep_1', 'dep_2']).setValue(1);
    expect(res.get(['dep_1', 'dep_2']).errors).toEqual({
      invalid_min_value: {
        msg: 'INVALID_MIN_NUMBER'
      }
    });
  });

  it('should return validate max text', () => {
    const res = service.setCtrlValidations(
      MOCK_FORM,
      { custom: [{ name: 'validatorTxtMaxLen', arg: [5] }] },
      'dep_1',
      'dep_2'
    );
    res.get(['dep_1', 'dep_2']).setValue('v');
    expect(res.get(['dep_1', 'dep_2']).errors).toBeNull();
  });
  it('should return invalidate max text', () => {
    const res = service.setCtrlValidations(
      MOCK_FORM,
      { custom: [{ name: 'validatorTxtMaxLen', arg: [5] }] },
      'dep_1',
      'dep_2'
    );
    res.get(['dep_1', 'dep_2']).setValue('invalid text length');
    expect(res.get(['dep_1', 'dep_2']).errors).toEqual({
      invalidLength: {
        msg: 'MAX_LEN'
      }
    });
  });

  it('should return validate min text', () => {
    const res = service.setCtrlValidations(
      MOCK_FORM,
      { custom: [{ name: 'validatorTxtMinLen', arg: [5] }] },
      'dep_1',
      'dep_2'
    );
    res.get(['dep_1', 'dep_2']).setValue('valid text length');
    expect(res.get(['dep_1', 'dep_2']).errors).toBeNull();
  });

  it('should return validate min text', () => {
    const res = service.setCtrlValidations(
      MOCK_FORM,
      { custom: [{ name: 'validatorTxtMinLen', arg: [5] }] },
      'dep_1',
      'dep_2'
    );
    res.get(['dep_1', 'dep_2']).setValue('v');
    expect(res.get(['dep_1', 'dep_2']).errors).toEqual({
      invalidLength: {
        msg: 'MIN_LEN'
      }
    });
  });
  it('should return validate carriage return string', () => {
    const mockForm = service.build_form({
      list: `uniq\nvalue\nlist`
    });
    const res = service.setCtrlValidations(
      mockForm,
      { custom: [{ name: 'validatorIsUnique' }] },
      'list'
    );
    expect(res.get(['list']).errors).toEqual(null);
  });
  it('should return invalidate duplicate carriage return string', () => {
    const mockForm = service.build_form({
      list: `dupli\ndupli\nlist`
    });
    const res = service.setCtrlValidations(
      mockForm,
      { custom: [{ name: 'validatorIsUnique' }] },
      'list'
    );
    expect(res.get(['list']).errors).toEqual({
      duplicate: {
        msg: 'IS_DUPLICATE'
      }
    });
  });
});
