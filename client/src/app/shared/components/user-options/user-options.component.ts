import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { PopoverService } from '@appServices/popover.service';
import { AppService } from '@appServices/app.service';
import { switchMap, takeUntil, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-options',
  templateUrl: './user-options.component.html',
  styleUrls: ['./user-options.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserOptionsComponent implements OnInit {
  private isDestroyed$: Subject<true> = new Subject<true>();
  constructor(
    private _popoverService: PopoverService,
    private _appService: AppService,
    private _router: Router
  ) {}

  ngOnInit(): void {}
  logout() {
    this._popoverService
      .dispose()
      .pipe(
        switchMap((_) => this._appService.logOut()),
        tap((_) => this.isDestroyed$.next(true)),
        takeUntil(this.isDestroyed$)
      )
      .subscribe();
  }
  profile() {
    this._popoverService
      .dispose()
      .pipe(
        switchMap((_) =>
          this._router.navigate(['/', 'app', 'profile', 'edit'])
        ),
        tap((_) => this.isDestroyed$.next(true)),
        takeUntil(this.isDestroyed$)
      )
      .subscribe();
  }
}
