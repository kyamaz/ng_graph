export function translatedRoute(routes: string): Array<string> {
  const _t = routes.split('/');
  const tail = _t.splice(1, _t.length); //[first, ...tail] = _t;
  return [...['/'], ...tail];
}
