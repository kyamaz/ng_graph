import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import {
  debounceTime,
  distinctUntilChanged,
  startWith,
  switchMap,
} from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { AppService } from '@appServices/app.service';
import { AppPerson } from '@models/app.model';

@Component({
  selector: 'app-auto-complete',
  templateUrl: './auto-complete.component.html',
  styleUrls: ['./auto-complete.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutoCompleteComponent implements OnInit {
  searchCtrl = new FormControl();
  filteredOptions: Observable<
    Pick<AppPerson, 'id' | 'firstName' | 'lastName'>[]
  >;
  constructor(private _appService: AppService) {}
  ngOnInit() {
    this.filteredOptions = this.searchCtrl.valueChanges.pipe(
      startWith(''),
      distinctUntilChanged(),
      debounceTime(500),
      switchMap((q) => this._appService.searchInName(q))
    );
  }
}
