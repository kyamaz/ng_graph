import { MD5 } from './utils';

export interface AuthPayload {
  email: string;
  password: string;
}

export const USER_AUTH = {
  access_token: 'mock_token',
  access_token_type: 'Bearer',
  business_id: 0,
  expires: new Date(new Date().getTime() + 1000 * 8400).toUTCString(),
  is_admin: false,
  user_id: 0,
  username: 'mockName',
};

export const CONST = {
  _expectedHash: MD5(`super_secret_token`),
  get expectedHash() {
    return this._expectedHash;
  },
  get expectedToken() {
    return `${MD5('Bearer ' + this._expectedHash)}`;
  },
};
