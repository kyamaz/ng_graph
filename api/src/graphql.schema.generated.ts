/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export class UpdateAppUser {
  userName?: string;
  email?: string;
  password?: string;
  firstName?: string;
  lastName?: string;
  avatar?: string;
}

export class AddAppUser {
  userName: string;
  email: string;
  password: string;
}

export class AppUserType {
  id: string;
  userName: string;
  email: string;
  token?: string;
  firstName?: string;
  lastName?: string;
  avatar?: string;
}

export class CompanyType {
  id: string;
  name: string;
  logo: string;
  phoneNumber?: string;
  people?: PersonType[];
}

export class PersonType {
  id: string;
  firstName: string;
  lastName: string;
  avatar?: string;
  jobTitle?: string;
  jobDescription?: string;
  phoneNumber?: string;
  company?: CompanyType;
}

export abstract class IQuery {
  abstract users(): AppUserType[] | Promise<AppUserType[]>;

  abstract user(id: string): AppUserType | Promise<AppUserType>;

  abstract registeredUser(
    password: string,
    email: string,
  ): AppUserType | Promise<AppUserType>;

  abstract persons(
    sort: string,
    order: string,
    limit: number,
    chunk: number,
  ): PersonType[] | Promise<PersonType[]>;

  abstract searchPersons(query: string): PersonType[] | Promise<PersonType[]>;

  abstract person(id: string): PersonType | Promise<PersonType>;

  abstract companies(): CompanyType[] | Promise<CompanyType[]>;

  abstract company(id: string): CompanyType | Promise<CompanyType>;
}

export abstract class IMutation {
  abstract updateUser(
    appUser: UpdateAppUser,
    id: string,
  ): AppUserType | Promise<AppUserType>;

  abstract createUser(appUser: AddAppUser): AppUserType | Promise<AppUserType>;

  abstract deleteAppUser(id: string): AppUserType | Promise<AppUserType>;
}

export abstract class ISubscription {
  abstract userUpdate(id: string): AppUserType | Promise<AppUserType>;
}
