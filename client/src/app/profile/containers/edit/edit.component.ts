import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { AppService } from '@appServices/app.service';
import { ReplaySubject } from 'rxjs';
import { UserGraph } from '@models/user.model';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditComponent implements OnInit {
  public user$: ReplaySubject<Partial<UserGraph>> = new ReplaySubject(1);

  constructor(private _appService: AppService) {}

  ngOnInit(): void {
    this.user$.next({
      userName: null,
      firstName: null,
      lastName: null,
      avatar: null,
    });

    this._appService
      .getUser(
        `userName
             firstName
             lastName
             avatar`
      )
      .pipe(
        tap(({ userName, firstName, lastName, avatar }) =>
          this.user$.next({
            userName,
            firstName,
            lastName,
            avatar,
          })
        )
      )
      .subscribe();
  }
}
