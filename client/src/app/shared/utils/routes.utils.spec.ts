import { translatedRoute } from 'src/app/shared/utils';

const MOCK_ROUTE = 'fr/mock/test';
describe('routes utils', () => {
  it('remove the first group', () => {
    let result = translatedRoute(MOCK_ROUTE);
    expect(result).toEqual(['/', 'mock', 'test']);
  });
});
