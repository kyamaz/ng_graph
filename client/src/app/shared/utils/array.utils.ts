import { PageEvent } from '@angular/material/paginator';
import { safeGet } from '@forms/customForms.utils';

export function filterList<T, U>(
  fullList: Array<string>,
  currentSelection: Array<T>,
  fn: Function = (src: U, tkt: T) => src === <any>tkt //pass conpare function. set a default function in case
): Array<string> {
  return fullList.reduce((acc: Array<string>, curr: string) => {
    if (currentSelection.find(sel => fn.call(null, sel, curr))) {
      return acc;
    }
    return [...acc, ...[curr]];
  }, []);
}

export function getPaginationeType(e: PageEvent): 'PREVIOUS' | 'NEXT' {
  const { previousPageIndex, pageIndex } = e;
  return previousPageIndex > pageIndex ? 'PREVIOUS' : 'NEXT';
}

/**
 * Moves an item one index in an array to another.
 * @param array Array in which to move the item.
 * @param fromIndex Starting index of the item.
 * @param toIndex Index to which the item should be moved.
 */
export function moveItemInArray<T = any>(
  array: T[],
  fromIndex: number,
  toIndex: number
): void {
  const from = clamp(fromIndex, array.length - 1);
  const to = clamp(toIndex, array.length - 1);

  if (from === to) {
    return;
  }

  const target = array[from];
  const delta = to < from ? -1 : 1;

  for (let i = from; i !== to; i += delta) {
    array[i] = array[i + delta];
  }

  array[to] = target;
}

/** Clamps a number between zero and a maximum. */
function clamp(value: number, max: number): number {
  return Math.max(0, Math.min(max, value));
}

export function makeImgsMap(
  obj: Partial<{ img: string }>,
  ...imgPath: Array<string>
): any {
  const root: string = imgPath[0];
  const imgs: Array<any> = safeGet(obj, ...imgPath);
  return {
    [root]: [imgs[0]]
  };
}

export function swapArrayItem<T = any>(
  currentArray: T[],
  targetArray: T[],
  currentIndex: number,
  targetIndex: number
): void {
  const to = clamp(targetIndex, targetArray.length);

  const newVal = currentArray[currentIndex];
  const SELECTED_ARR_LASTD_POS = 2;
  const correctedIdx = isNaN(to - 1) ? SELECTED_ARR_LASTD_POS : to - 1;
  if (currentArray.length) {
    targetArray.splice(correctedIdx, 1, newVal);
  }
}

export function setViewportHeight<T>(
  list: Array<T>,
  ITEMS_PER_ROW: number,
  MAX_HEIGHT_IN_REM: number = 45
): number {
  const ROW_HEIGHT_IN_REM: number = 17;
  const rowNb = Math.ceil(list.length / ITEMS_PER_ROW);
  return rowNb * ROW_HEIGHT_IN_REM >= MAX_HEIGHT_IN_REM
    ? MAX_HEIGHT_IN_REM
    : rowNb * ROW_HEIGHT_IN_REM;
}
