import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import {
  DefaultState,
  InitState,
  SigninMachineService,
  UpdateState,
} from '@login/services/signin-machine.service';
import { delay, distinctUntilChanged, map, tap } from 'rxjs/operators';
import { Observable, ReplaySubject } from 'rxjs';
import { AuthMachineService } from '@login/services/auth.machine.service';

export type LoginType = 'signup' | 'signin';
const TRANSITION_OFFSET = 500;

enum signup {
  asideTitle = `Welcome back!`,
  toggleBtn = 'Sign in',
  content = `
    Enter your personal details to keep connected with us
    `,
}
enum signin {
  asideTitle = `
    Join us!`,
  toggleBtn = 'Sign up',
  content = `
    Enter your personal details to start with us
    `,
}

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent implements OnInit {
  public _loginType$: ReplaySubject<LoginType> = new ReplaySubject<LoginType>(
    1
  );
  public loginType$: ReplaySubject<LoginType> = new ReplaySubject<LoginType>(1);
  public position$: Observable<{ dirty: boolean; isTranslated: boolean }>;

  constructor(
    private _signinMachineService: SigninMachineService,
    private _authMachineService: AuthMachineService
  ) {}

  ngOnInit(): void {
    this._signinMachineService.start();
    this._authMachineService.start();
    this.position$ = this._signinMachineService.authState$.pipe(
      distinctUntilChanged((prev, curr) => prev.context === curr.context),
      map((s) => s.context),
      tap((state) => {
        const nxState: LoginType = state.isTranslated ? 'signup' : 'signin';
        this._loginType$.next(nxState);
      })
    );

    this._loginType$
      .pipe(
        delay(TRANSITION_OFFSET),
        tap((state) => this.loginType$.next(state))
      )
      .subscribe();
  }

  toggle(position): void {
    if (!position.dirty) {
      this._signinMachineService.send(new InitState());
      this._signinMachineService.send(new UpdateState());
      return;
    }

    if (position.isTranslated) {
      this._signinMachineService.send(new DefaultState());
    } else {
      this._signinMachineService.send(new UpdateState());
    }
  }

  toggleLogin(logType: LoginType, node: string): LoginType {
    return logType === 'signup' ? signup[node] : signin[node];
  }
}
