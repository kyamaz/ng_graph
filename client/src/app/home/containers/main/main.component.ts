import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AppPerson } from '@models/app.model';
import { map, tap } from 'rxjs/operators';
import {
  DataMachineService,
  DataState,
  FetchMore,
  InitData,
} from '@appServices/data-machine.service';
export interface AppPersonDataState {
  state: DataState;
  data: Array<Omit<AppPerson, 'phoneNumber'>[]>;
}
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainComponent implements OnInit {
  public dataState$: Observable<AppPersonDataState>;

  constructor(
    private _dataMachineService: DataMachineService<
      Omit<AppPerson, 'phoneNumber'>
    >
  ) {}

  ngOnInit() {
    this._dataMachineService.start();
    this._dataMachineService.send(new InitData());

    this.dataState$ = this._dataMachineService.dataState$.pipe(
      map((machineState) => {
        return {
          state: machineState.value as DataState,
          data: machineState.context.data,
        };
      })
    );
  }

  refetch(_) {
    this._dataMachineService.send(new FetchMore());
  }
}
