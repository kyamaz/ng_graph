import {
  firstLetter,
  normaliseKey,
  normalizeStrToCompare,
  StringsUtils
} from 'src/app/shared/utils';

const MOCK_ROUTE = 'fr/mock/test';
describe('srings utils', () => {
  it('should trim and lowercase inputs', () => {
    let result = normaliseKey(' MOCK_ROUTE ');
    expect(result).toEqual('mock_route');
  });

  it('should normalize inpust', () => {
    let result = normalizeStrToCompare('mock dédé');
    expect(result).toEqual('mock_dede');
  });

  it('should return the first character', () => {
    let result = firstLetter('mock');
    expect(result).toEqual('m');
  });

  it('should get  every content betwween characters', () => {
    const MOCK_CONTENT =
      'mock {first content}, {second content} and more content';
    const MOCK_BETWEEN = ['first content', 'second content'];
    let result = StringsUtils.getFromBetween.getValues(MOCK_CONTENT, '{', '}');

    expect(result).toEqual(MOCK_BETWEEN);
  });

  it('should flatten array', () => {
    const MOCK_NESTED_LIST = [[1], 2, [[3, 4], 5], [[[]]], [[[6]]], 7, 8, []];
    const MOCK_FLATTEN_LIST = [1, 2, 3, 4, 5, 6, 7, 8];
    let result = StringsUtils.flatten(MOCK_NESTED_LIST);

    expect(result).toEqual(MOCK_FLATTEN_LIST);
  });

  it('should recursively split string', () => {
    const MOCK_CONTENT =
      'mock {first content}, {second content} and more content';
    const MOCK_RESULTS = [
      'mock ',
      'first content}, ',
      'second content} and more content'
    ];
    let result = StringsUtils.splitString(MOCK_CONTENT, '{');

    expect(result).toEqual(MOCK_RESULTS);
  });

  it('should recursively apply function to  string', () => {
    let count = 0;
    const MOCK_CONTENT =
      'mock {first content}, {second content} and {more content';
    const mockFunction = (el, exp) => {
      count++;
      return el.split(exp);
    };
    let result = StringsUtils.mapFuncToString(MOCK_CONTENT, '{', mockFunction);
    expect(count).toEqual(result.length + 2);
  });

  it('should return undefined', () => {
    const MOCK_CONTENT =
      'mock {first content}, {second content} and {more content';

    let result = StringsUtils.getFromBetween.getValues(null, 'mock', 'second');
    expect(result).toBe(undefined);
  });
});
