import { environment } from 'environments/environment';
//API
export const API_ROOT: string = environment.url;
export const LOCAL_STORAGE_KEY = '_ng_graph_';
