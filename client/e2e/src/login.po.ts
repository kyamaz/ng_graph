import { browser, by, element } from 'protractor';

export class LoginPage {
  navigateTo() {
    return browser.get('/login') as Promise<any>;
  }

  getElById(tag: string, id: string) {
    return element(by.css(`${tag}[data-testid=${id}]`));
  }

  getTitleText() {
    return element(by.css(`h2`)).isPresent();
  }

  isSubmitBtnDisabled() {
    return element(by.id('submit')).getAttribute('disabled');
  }

  getInput(id) {
    return this.getElById('input', id);
  }

  setFieldValue(id: string, value: string) {
    const input = this.getInput(id);
    input.clear().then(() => {
      input.sendKeys(value);
    });
  }

  trySignin(email: string, pwd: string) {
    this.setFieldValue('email', email);
    this.setFieldValue('pwd', pwd);
    const emailInput = this.getElById('input', 'email');
    const pwdInput = this.getElById('input', 'pwd');

    emailInput
      .clear()
      .then(() => {
        emailInput.sendKeys(email);
      })
      .then(_ => pwdInput.clear())
      .then(_ => pwdInput.sendKeys(pwd))
      .then(_ => element(by.id('submit')).click());
  }

  getFieldValidity(id: string) {
    return this.getElById('input', id).getAttribute('aria-invalid');
  }

  hasErrorMsg(name: string) {
    return element(by.className(name)).isPresent();
  }
}
