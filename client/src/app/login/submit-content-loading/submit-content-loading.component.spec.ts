import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitContentLoadingComponent } from './submit-content-loading.component';

describe('SubmitContentLoadingComponent', () => {
  let component: SubmitContentLoadingComponent;
  let fixture: ComponentFixture<SubmitContentLoadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SubmitContentLoadingComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitContentLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
