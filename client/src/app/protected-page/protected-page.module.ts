import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';
import {ROUTES} from './protected-page.routes';
import {PageComponent} from './container/page/page.component';

//components

@NgModule({
  declarations: [PageComponent],
  imports: [CommonModule, SharedModule, RouterModule.forChild(ROUTES)]
})
export class ProtectedPageModule {
}
