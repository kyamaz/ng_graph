import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitContentSuccessComponent } from './submit-content-success.component';

describe('SubmitContentSuccessComponent', () => {
  let component: SubmitContentSuccessComponent;
  let fixture: ComponentFixture<SubmitContentSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SubmitContentSuccessComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitContentSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
