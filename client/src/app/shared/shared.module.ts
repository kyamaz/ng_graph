import { CommonModule } from '@angular/common';
import { CustomFormsModule } from '@forms/customForms.module';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
//angular material
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { PortalModule } from '@angular/cdk/portal';
import { OverlayModule } from '@angular/cdk/overlay';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
//Components
import { AppNavComponent } from '@components/app-nav/app-nav.component';
import { StateBtnComponent } from '@components/state-btn/state-btn.component';
import { NotFoundComponent } from '@components/not-found/not-found.component';
import { ThemeToggleComponent } from './components/theme-toggle/theme-toggle.component';
import { UserActionComponent } from './components/user-action/user-action.component';
import { UserOptionsComponent } from './components/user-options/user-options.component';

@NgModule({
  declarations: [
    StateBtnComponent,
    NotFoundComponent,
    AppNavComponent,
    ThemeToggleComponent,
    UserActionComponent,
    UserOptionsComponent,
  ],
  bootstrap: [],
  imports: [
    CommonModule,
    CustomFormsModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatListModule,
    MatTooltipModule,
    MatSlideToggleModule,
    MatButtonModule,
    MatSelectModule,
    PortalModule,
    OverlayModule,
    MatInputModule,
    MatTabsModule,
    ScrollingModule,
    MatAutocompleteModule,
  ],
  exports: [
    CustomFormsModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatListModule,
    MatTooltipModule,
    MatSlideToggleModule,
    MatButtonModule,
    MatSelectModule,
    PortalModule,
    MatInputModule,
    MatTabsModule,
    ScrollingModule,
    MatAutocompleteModule,
    //components
    StateBtnComponent,
    AppNavComponent,
    NotFoundComponent,
    ThemeToggleComponent,
  ],
})
export class SharedModule {}
