import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { CanActivate, CanDeactivate, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Apollo } from 'apollo-angular';
import { LOCAL_STORAGE_KEY } from '../const';

@Injectable({
  providedIn: 'root',
})
export class RoutesGuardService implements CanActivate {
  constructor(
    private _router: Router,
    private _apollo: Apollo,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {}

  canActivate(): Observable<boolean> {
    const isAuth =
      !!localStorage.getItem(LOCAL_STORAGE_KEY) &&
      JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY)).token;
    return of(isAuth).pipe(
      switchMap((auth) => {
        if (!auth) {
          localStorage.removeItem(LOCAL_STORAGE_KEY);
          return this._router.navigate(['/', 'login']);
        }
        return of(true);
      })
    );
    //angular universal
    /*     if (isPlatformBrowser(this.platformId)) {
      if (window !== undefined) {
        this._router.navigate(['/', 'login']);
        return of(false);

        const userCreds = !!this._appService.userResponse.access_token
          ? this._appService.userResponse
          : JSON.parse(localStorage.getItem('naister_user'));

        if (!this._appService.userResponse) {
          localStorage.setItem('naister_user', JSON.stringify(userCreds));
        }
        this.has_logged = !!userCreds.access_token;

        if (this.has_logged) {
          const startDate = new Date().getTime();
          // Do your operations
          const endDate = new Date(userCreds.expires).getTime();

          const remaining = endDate - startDate;
          const remainingToMin = Math.floor(remaining / 60000);

          if (remainingToMin < 10) {
            return this._authService
              .checkAuthentication(userCreds.access_token)
              .pipe(
                map(response => {
                  if (response) {
                    return true;
                  }
                  this._router.navigate(['/', 'login']);
                  return false;
                })
              );
          }

          return of(true);
        }
      }
      this._router.navigate(['/', 'login']);
      return of(false);


    }*/
  }
}

export class LeaveAppGuard implements CanDeactivate<any> {
  constructor() {}

  canDeactivate(component: any) {
    return component.logOut();
  }
}
