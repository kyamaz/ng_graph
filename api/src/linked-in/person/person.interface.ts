import { Document } from 'mongoose';
import { Company } from '../company/company.interface';
export interface BasePerson extends Document {
  id: string;
  firstName: string;
  lastName: string;
  avatar: string;
  jobTitle: string;
  jobDescription: string;
  phoneNumber: string;
}
export interface Person extends BasePerson {
  companyId: string;
}

export interface ViewPerson extends BasePerson {
  company: Company;
}
