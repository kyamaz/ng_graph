import * as mongoose from 'mongoose';
import { strict } from 'assert';

export const PersonSchema = new mongoose.Schema({
  id: String,
  firstName: String,
  lastName: String,
  avatar: String,
  jobTitle: String,
  jobDescription: String,
  phoneNumber: String,
  companyId: String,
});
