import { HttpInterceptService } from './shared/services/http.intercept.service';
import {
  HTTP_INTERCEPTORS,
  HttpClient,
  HttpClientModule,
} from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';

import { LoginModule } from './login/login.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
// module
import { SharedModule } from './shared/shared.module';
// root routes
import { AppRoutingModule } from '@routes/app-routing.module';
//graphql
import { APOLLO_OPTIONS, ApolloModule } from 'apollo-angular';
import { HttpLink, HttpLinkModule } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloLink } from 'apollo-link';
import { setContext } from 'apollo-link-context';
import { LOCAL_STORAGE_KEY } from './shared/const';

const GRAPHQL_URI = 'graphql';

//"http://localhost:5001/graphql"

export function provideApollo(httpLink: HttpLink) {
  const basic = setContext((operation, context) => ({
    headers: {
      Accept: 'charset=utf-8',
    },
  }));

  // Get the authentication token from local storage if it exists
  const _tk = localStorage.getItem(LOCAL_STORAGE_KEY);
  const token = _tk ? JSON.parse(_tk).token : '';
  const auth = setContext((operation, context) => ({
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }));
  // Create a WebSocket link:
  /*  const ws = new WebSocketLink({
    uri: `ws://localhost:5000/`,
    options: {
      reconnect: true,
    },
  });

  const _links = split(
    ({ query }) => {
      // @ts-ignore
      const { kind, operation } = getMainDefinition(query);
      return kind === 'OperationDefinition' && operation === 'subscription';
    },
    ws,
    httpLink.create({ uri: GRAPHQL_URI })
  );*/
  const link = ApolloLink.from([
    basic,
    auth,
    httpLink.create({ uri: GRAPHQL_URI }),
  ]);
  const cache = new InMemoryCache();

  return {
    link,
    cache,
  };
}
@NgModule({
  declarations: [AppComponent],
  imports: [
    CommonModule,
    SharedModule,
    BrowserAnimationsModule,
    LoginModule,
    HttpClientModule,
    AppRoutingModule,
    ApolloModule,
    HttpLinkModule,
  ],
  providers: [
    HttpClient,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptService,
      multi: true,
    },
    {
      provide: APOLLO_OPTIONS,
      useFactory: provideApollo,
      deps: [HttpLink],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
