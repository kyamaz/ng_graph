# Mercari

## Start guide

first be sure to have docker installed.  
in the terminal, type `docker -v`.  
you sould see the docker version. If not, you have to install it.
More detail at this link :https://runnable.com/docker/install-docker-on-macos

### Starting dev server

- at the root of project, type in the terminal type
  `npm run docker:start`
- when the installation is completed, go to :  
   http://localhost/

### Starting prod server

- at the root of project, type in the terminal type
  `npm run docker:prod`
- when the installation is completed, go to :  
   http://localhost/

### Stoping the server

- at the root of project, type in the terminal type
  `npm run docker:stop`

### Trouble shooting

- if you see a gateway error, it may be caused by starting a dev then prod server.
  Sometimes Dockertry to reuses invalid caches, one way is to clear all caches and rebuild every images.
  `run docker:prune` then `docker:prod-rebuild` or `docker:rebuild` (for the dev server)
