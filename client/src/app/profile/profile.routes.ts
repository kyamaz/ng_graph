import { Routes } from '@angular/router';
import { EditComponent } from './containers/edit/edit.component';

export const ROUTES: Routes = [
  {
    path: 'edit',
    component: EditComponent,
  },
];
