import { AppService } from './app.service';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { WindowRef } from 'src/app/win_ref';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { LOCAL_STORAGE_KEY } from '../const';

function setHeaderClient(
  request: HttpRequest<any>,
  payload: string
): HttpRequest<any> {
  const _req = !!payload
    ? {
        Authorization: payload,
      }
    : {};

  return request.clone({
    setHeaders: _req,
  });
}

@Injectable({
  providedIn: 'root',
})
export class HttpInterceptService implements HttpInterceptor {
  constructor(
    private _router: Router,
    private winRef: WindowRef,
    private _authService: AuthService,
    private _appService: AppService
  ) {}

  getUserToken = (): string => {
    const _tk: string | null = localStorage.getItem(LOCAL_STORAGE_KEY);
    const localToken: string = !!_tk ? JSON.parse(_tk).token : '';
    return !!this._appService.userResponse
      ? this._appService.userResponse.token
      : localToken;
  };

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    //set req headers

    const auth = !!this.getUserToken() ? `Bearer ${this.getUserToken()}` : null;
    const newReq = setHeaderClient(request, auth);

    return next.handle(newReq).pipe(
      tap(
        (event: HttpEvent<any>) => {
          /*   if (event instanceof HttpResponse) {
          } */
        },
        (err: any) => {
          if (err instanceof HttpErrorResponse) {
            if (err.status === 401) {
              localStorage.removeItem(LOCAL_STORAGE_KEY);
              return this._router.navigate(['/', 'login']);
            }
          }
        }
      )
    );
  }
}
