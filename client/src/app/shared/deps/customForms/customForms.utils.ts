import * as _cloneDeep from 'lodash/fp/cloneDeep';

export { _cloneDeep };

export function isObject<T extends any>(obj: T): boolean {
  var type = typeof obj;
  return (
    obj === Object(obj) &&
    Object.prototype.toString.call(obj) !== '[object Array]'
  );
}

export function safeGet<T extends Object>(value: Object, ...path: string[]): T {
  return path.reduce((prev: Object, prop: string) => {
    if (prev && prev.hasOwnProperty(prop)) {
      return prev[prop];
    } else {
      return null;
    }
  }, value);
}

export function fieldsAreEqual(
  form: { [key: string]: any },
  keys: [string, string],
  error: { [key: string]: true }
): { [key: string]: true } {
  const [source, confirm] = keys;
  return form[source] === form[confirm] ? null : error;
}
