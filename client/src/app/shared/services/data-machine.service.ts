import { Injectable } from '@angular/core';
import { EventObject, interpret, Machine, MachineOptions, State } from 'xstate';
import { AppService } from '@appServices/app.service';
import { catchError, map } from 'rxjs/operators';
import { fromEventPattern, Observable, of } from 'rxjs';
import { AppPerson } from '@models/app.model';
import { SortKey, SortOrder, SortState } from '@models/index';
// chunk list in pair of 3 && group them into a component to make virtal scroll work
// without css hacking
function flatten<T>(list: Array<T[]>) {
  return list.reduce((acc: T[], current: T[]) => {
    return acc.concat(...current);
  }, []);
}
export function chunkArray<T>(list: Array<T>, chunkSize = 3): Array<T[]> {
  let chunkLen = 0;
  return list.reduce((acc: Array<T[]>, curr, idx) => {
    if (idx % chunkSize === 0) {
      const _newBatch = [curr];
      chunkLen++;
      acc.push(_newBatch);
      return acc;
    }
    const _activeBatch = acc[chunkLen - 1];
    _activeBatch.push(curr);

    return acc;
  }, []);
}
interface DataContext<T> {
  retry: number;
  data: Array<T[]>;
  error: string;
  chunk: number;
  sort: { key: SortKey; order: SortOrder };
}
export type DataState =
  | 'idle'
  | 'loading'
  | 'loadDataSuccess'
  | 'loadDataFailed'
  | 'hasData'
  | 'hasNoData';

export type DataEvent =
  | InitData
  | FetchMore
  | LoadDataSuccess
  | LoadDataFailed
  | HasData
  | HasNoData
  | SortBy;

interface DataStateSchema {
  states: {
    idle: {};
    loading: {};
    loadDataSuccess: {};
    loadDataFailed: {};
    hasData: {};
    hasNoData: {};
  };
}
const INIT_DATUM: Omit<AppPerson, 'phoneNumber'> = {
  _typename: null,
  avatar: null,
  company: { name: null, logo: null, id: null, people: [] },
  firstName: null,
  id: null,
  jobDescription: null,
  jobTitle: null,
  lastName: null,
};
const INIT_DATA = [
  [INIT_DATUM, INIT_DATUM, INIT_DATUM],
  [INIT_DATUM, INIT_DATUM, INIT_DATUM],
  [INIT_DATUM, INIT_DATUM, INIT_DATUM],
];
const dataMachineConfig = {
  id: 'dataMachine',
  initial: 'idle' as DataState,
  context: {
    retry: 0,
    chunk: 0,
    data: INIT_DATA,
    error: null,
    sort: {
      key: '_id',
      order: 'asc',
    },
  },
  states: {
    idle: {
      on: {
        LOADING: {
          target: 'loading',
        },
      },
    },

    loading: {
      invoke: {
        id: 'dataLoading',
        src: 'dataLoading',
      },
      on: {
        LOAD_SUCCESS: {
          target: 'loadDataSuccess',
          actions: ['loadDataSuccess'],
        },
        LOAD_FAILED: { target: 'loadDataFailed' },
      },
    },
    loadDataSuccess: {
      invoke: {
        id: 'dataLoaded',
        src: 'dataLoaded',
      },
      on: {
        HAS_DATA: {
          target: 'hasData',
        },
        HAS_NO_DATA: {
          target: 'hasNoData',
        },
      },
    },
    hasData: {
      on: {
        FETCH_MORE: {
          target: 'loading',
          actions: ['more'],
        },
        SORT_BY: {
          target: 'loading',
          actions: ['sort'],
        },
      },
    },
    hasNoData: {
      type: 'final',
    },
    loadDataFailed: {
      type: 'final',
    },
  },
};

export class InitData {
  readonly type = 'LOADING';
}

export class FetchMore {
  readonly type = 'FETCH_MORE';
}
export class SortBy {
  readonly type = 'SORT_BY';
  constructor(public sort: { key: SortKey; order: SortOrder }) {}
}
export class LoadDataSuccess {
  readonly type = 'LOAD_SUCCESS';
  constructor(public data: [any[]]) {}
}
export class LoadDataFailed {
  readonly type = 'LOAD_FAILED';
  constructor(public errorMsg: string) {}
}
export class HasData {
  readonly type = 'HAS_DATA';
  constructor() {}
}
export class HasNoData {
  readonly type = 'HAS_NO_DATA';
  constructor() {}
}
@Injectable({
  providedIn: 'root',
})
export class DataMachineService<T> {
  dataMachineOption: Partial<MachineOptions<DataContext<T>, DataEvent>> = {
    services: {
      dataLoading: (ctx) => {
        return this._appService
          .getPersons(ctx.chunk, ctx.sort.order, ctx.sort.key)
          .pipe(
            map((data: any) => {
              // first load (chunk 0), ignore dummy values
              // else just flatten and concat with existing
              // flat values else all chunk won't have same size
              const ctxData =
                ctx.chunk === 0 ? data : flatten(ctx.data).concat(data);
              const chunk: any = chunkArray(ctxData);
              return new LoadDataSuccess(chunk);
            }),
            catchError((err) => {
              console.log(err);
              return of(new LoadDataFailed('server error whatever'));
            })
          );
      },

      dataLoaded: (ctx) => {
        if (ctx.data.length === 0) {
          return of(new HasNoData());
        }
        return of(new HasData());
      },
    },
    actions: {
      loadDataSuccess: (ctx, event: LoadDataSuccess) => {
        ctx.data = event.data;
      },

      sort: (
        ctx,
        { sort: { key, order } }: { type: 'SORT_BY'; sort: SortState }
      ) => {
        ctx.sort = { key, order };
        ctx.chunk = 0;
        ctx.data = INIT_DATA as any;
      },
      more: (ctx) => {
        ctx.chunk = ctx.chunk + 1;
      },
    },
  };
  private _dataMachine = Machine<DataContext<T>, DataStateSchema, DataEvent>(
    dataMachineConfig as any
  ).withConfig(this.dataMachineOption);
  // @ts-ignore
  private _service = interpret(this._dataMachine, { devTools: true }).start();
  // State<AuthContext, AuthEvent>
  dataState$: Observable<
    State<DataContext<T>, DataEvent, DataStateSchema>
  > = fromEventPattern<[any, EventObject]>(
    (handler) => {
      return this._service.onTransition(handler);
    },
    (_, service) => {
      return service.stop();
    }
  ).pipe(map(([state, _]) => state));
  send(event: any) {
    this._service.send(event);
  }

  start() {
    this._service.start();
  }

  constructor(private _appService: AppService) {}
}
