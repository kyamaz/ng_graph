import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { AppPersonDataState } from '../../containers/main/main.component';
import { ScrollDispatcher } from '@angular/cdk/overlay';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { distinctUntilChanged, skip, tap } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';
import { AppService } from '@appServices/app.service';

@Component({
  selector: 'person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PersonListComponent implements OnInit {
  @Input() dataState: AppPersonDataState;
  @Output() onRefetch = new EventEmitter<number>();
  @ViewChild(CdkVirtualScrollViewport)
  viewPort: CdkVirtualScrollViewport;
  private _fetchMore$: ReplaySubject<any> = new ReplaySubject(1);
  constructor(
    private _scrollDispatcher: ScrollDispatcher,
    private _appService: AppService
  ) {}

  ngOnInit() {
    this._fetchMore$
      .pipe(
        distinctUntilChanged((prev, curr) => {
          return curr < prev;
        }),
        skip(1),
        tap((end: number) => {
          this.onRefetch.emit(end);
        })
      )
      .subscribe();
  }
  loadMore(_) {
    const end = this.viewPort.getRenderedRange().end;
    const total = this.viewPort.getDataLength();

    if (end === total) {
      this._fetchMore$.next(end);
    }
  }
}
