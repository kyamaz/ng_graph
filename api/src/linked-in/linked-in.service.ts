import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Query, QueryCursor } from 'mongoose';
import { Person, ViewPerson } from './person/person.interface';
import { Company, ViewCompany } from './company/company.interface';
// tslint:disable-next-line:no-var-requires
const faker = require('faker');

@Injectable()
export class LinkedInService {
  constructor(
    @InjectModel('Person') private _personModel: Model<Person>,
    @InjectModel('Company') private _companyModel: Model<Company>,
  ) {
    /*    this.clearAll().then((res) => {
      console.log(res);
    });*/
    this.populateCollections().then((res) => console.log(res));
  }
  async clearAll() {
    const resPerson = await this._personModel.remove({});
    const resCompany = await this._companyModel.remove({});
    return {
      deletedPerson: resPerson.deletedCount,
      deletedCompany: resCompany.deletedCount,
    };
  }
  async populateCollections() {
    const personCount = await this._personModel.countDocuments({});
    const companyCount = await this._companyModel.countDocuments({});

    if (personCount > 0 && companyCount > 0) {
      return { personCount, companyCount };
    }
    if (personCount === 0) {
      await this._populatePerson();
    }
    if (companyCount === 0) {
      await this._populateCompany();
    }

    return 'Collections populated';
  }

  async _populatePerson() {
    for (let i = 0; i < 100; i++) {
      const _person = new this._personModel({
        id: i,
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        avatar: faker.internet.avatar(),
        jobTitle: faker.name.jobTitle(),
        jobDescription: faker.name.jobDescriptor(),
        phoneNumber: faker.phone.phoneNumber(),
        companyId: Math.floor(Math.random() * 30) + 1,
      });
      _person.save();
    }
  }
  async _populateCompany() {
    for (let i = 0; i < 31; i++) {
      let _personIds = [];
      const numPersons = Math.floor(Math.random() * 20);
      for (let i = 0; i < numPersons; i++) {
        _personIds.push(Math.floor(Math.random() * 99) + 1);
      }
      const _company = new this._companyModel({
        id: i,
        name: faker.company.companyName(),
        logo: faker.image.business(),
        phoneNumber: faker.phone.phoneNumber(),
        personIds: _personIds,
      });
      _company.save();
    }
  }
  async getPersonList(
    chunk,
    limit = 20,
    order = 'asc',
    sort = '_id',
  ): Promise<ViewPerson[]> {
    const query: Query = await this._personModel
      .find()
      .sort({ [sort]: order })
      .skip(chunk * limit)
      .limit(limit)
      .setOptions({ lean: true });

    return query;
  }

  async queryPerson(q: string): Promise<ViewPerson[]> {
    const query: Query = await this._personModel
      .find({
        $or: [
          { firstName: { $regex: q, $options: 'i' } },
          { lastName: { $regex: q, $options: 'i' } },
        ],
      })
      .setOptions({ lean: true });
    return query;
  }

  async getPerson(id: string): Promise<ViewPerson> {
    return await this._personModel.findOne({ id });
  }

  async getCompanyList(): Promise<Company[]> {
    return await this._companyModel.find();
  }

  async getCompany(id: string): Promise<Company> {
    return await this._companyModel.findOne({ id });
  }

  async getCompanyPerson(ids: string[]): Promise<Person[]> {
    const persons = await Promise.all(
      ids.map((id) => this._personModel.findOne({ id })),
    );
    return persons;
  }
}
