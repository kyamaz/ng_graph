import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core';

interface BtnState {
  class: string;
  txt: string;
  isPending: boolean;
  isDone: boolean;
  icon: string;
  className: string;
}

export type StateButtonType =
  | 'UPLOAD'
  | 'INITIAL'
  | 'PENDING'
  | 'SUCCESS'
  | 'ERROR';

@Component({
  selector: 'state-btn',
  templateUrl: './state-btn.component.html',
  styleUrls: ['./state-btn.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StateBtnComponent implements OnChanges {
  @Input() state: StateButtonType;
  @Input() valid: 'VALID' | 'INVALID';
  @Input() type: 'submit' | 'button';
  @Input() attr: string;
  @Input() customStyle: {};

  public readonly stateMap: { [prop: string]: BtnState } = {
    INITIAL: {
      class: 'initial',
      txt: 'submit',
      isPending: false,
      isDone: false,
      icon: null,
      className: null
    },
    UPLOAD: {
      class: 'initial',
      txt: 'upload',
      isPending: false,
      isDone: false,
      icon: null,
      className: null
    },

    PENDING: {
      class: 'pending',
      txt: '',
      isPending: true,
      isDone: false,
      icon: null,
      className: null
    },
    SUCCESS: {
      class: 'success',
      txt: 'success',
      isPending: false,
      isDone: true,
      icon: 'done',
      className: 'success'
    },
    ERROR: {
      class: 'error',
      txt: 'error',
      isPending: false,
      isDone: true,
      icon: 'error',
      className: 'error'
    }
  };
  public btnStyle: {} = {};

  constructor() {}

  ngOnChanges(sc: SimpleChanges) {
    /*   if (sc.btn) {
      this.btnStyle = { ...this.btnStyle, ...{ color: sc.btn.currentValue.color } };
    } */
    if (sc.customStyle) {
      this.btnStyle = {
        ...this.btnStyle,
        ...JSON.parse(sc.customStyle.currentValue)
      };
    }
  }
}
