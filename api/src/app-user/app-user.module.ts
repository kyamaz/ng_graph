import { Module } from '@nestjs/common';
import { AppUserService } from './app-user.service';
import { AppUserResolver } from './app-user.resolver';
import { MongooseModule } from '@nestjs/mongoose';
import { AppUserSchema } from './app-user.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'AppUser', schema: AppUserSchema }]),
  ],
  providers: [AppUserService, AppUserResolver],
})
export class AppUserModule {}
