import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class AppUserType {
  @Field(() => ID, { description: 'register user id' })
  readonly id: string;
  @Field(() => String, { description: 'register user display name' })
  readonly userName: string;
  @Field(() => String, { description: 'register user email' })
  email: string;
  @Field(() => String, { description: 'token', nullable: true })
  token?: string;
  @Field(() => String, {
    description: 'register user first name',
    nullable: true,
  })
  firstName: string;
  @Field(() => String, {
    description: 'register user last name',
    nullable: true,
  })
  lastName: string;
  @Field(() => String, { description: 'register user avatar', nullable: true })
  avatar: string;
}
