import { LoginPage } from './login.po';
import { browser } from 'protractor';

describe('[naister] Login', () => {
  let page: LoginPage;

  beforeEach(() => {
    page = new LoginPage();
    page.navigateTo();
  });

  it('should display description', async () => {
    const title = await page.getTitleText();
    expect(title).toEqual(true);
  });
  it('should disable submit button', () => {
    expect(page.isSubmitBtnDisabled()).toEqual('true');
  });
  it('should invalid email', () => {
    page.setFieldValue('email', 'totot');
    page.getInput('pwd').click();
    expect(page.getFieldValidity('email')).toEqual('true');
  });
  it('should display invalid message', () => {
    page.setFieldValue('pwd', 'totot');
    page.getInput('email').click();
    expect(page.hasErrorMsg('login-form__invalid-msg')).toEqual(true);
  });

  it('should prevent submit invalid  email', () => {
    page.setFieldValue('email', 'totot');
    page.setFieldValue('pwd', 'totottotottotot');
    expect(page.isSubmitBtnDisabled()).toEqual('true');
  });
  it('should prevent submit invalid  pwd', () => {
    page.setFieldValue('email', 'totot@copop.fr');
    page.setFieldValue('pwd', 'totot');
    expect(page.isSubmitBtnDisabled()).toEqual('true');
  });
  it('should display message on invalid credentials', () => {
    page.trySignin('totot@copop.fr', 'tototeeeeeeee');
    expect(page.hasErrorMsg('error__msg')).toEqual(true);
  });
  it('should redirect  to app on valid credentials', () => {
    page.trySignin('dlp@naister.com', 'naisternaister');

    browser.wait(function() {
      return browser.getCurrentUrl().then(function(currentUrl) {
        return expect(currentUrl.includes('app'));
      });
    }, 1000);
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    /*     const logs = await browser
      .manage()
      .logs()
      .get(logging.Type.BROWSER);
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE
      } as logging.Entry)
    ); */
  });
});
