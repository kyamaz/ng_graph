import { HttpTestingController } from '@angular/common/http/testing';

export const MOCK_GENERIC_SERVICE_RESPONSE = {
  resp: true
};
export const MOCK_USER_SERVICE_RESPONSE = {
  resp: {
    access_token: 'fakse',
    user_name: 'fakse',
    user_id: 0,
    is_admin: false,
    business_id: 0,
    access_token_type: 'mock',
    expires: new Date().toUTCString()
  }
};

export const MOCK_CREDS = { email: 'fake@fake.fr', password: 'fakefake' };

export const MOCK_VERSION = {
  created_date: Date.now(),
  updated_date: Date.now(),
  created_date_pretty: Date.now(),
  updated_date_pretty: Date.now(),
  name: 'mock',
  description: 'mock',
  status: 'mock',
  idcontainer: '0',
  idcontainerversion: 0,
  idsite: 0,
  revision: 0,
  environments: ['live', 'preview']
};
export const MOCK_CONTAINER = {
  idcontainer: '0',
  idsite: 0,
  context: 'web',

  versions: [MOCK_VERSION]
};
export const MOCK_SITE = {
  idsite: '0',
  name: 'mock',
  main_url: 'mock',
  ts_created: Date.now(),
  ecommerce: 'mock',
  sitesearch: 'mock',
  sitesearch_keyword_parameters: 'mock',
  sitesearch_category_parameters: 'mock',
  timezone: 'mock',
  currency: 'mock',
  exclude_unknown_urls: 'mock',
  excluded_ips: 'mock',
  excluded_parameters: 'mock',
  excluded_user_agents: 'mock',
  group: 'mock',
  type: 'website',
  keep_url_fragment: 'mock',
  creator_login: 'mock',
  timezone_name: 'mock'
};

export const MOCK_PRODUCT_CATALOG = {
  id: 0,
  catalog_file: 'mock',
  name: 'first',
  number_products: 0,
  processed_products: 0,
  status: 'mock',
  delimiter: 'mock',
  last_update: 'mock',
  user_id: 0,
  fields: ['mock'],
  business: 0
};

//utils
export function debugUrl(httpMock: HttpTestingController, MOCK_URL: string) {
  httpMock.expectOne(req => {
    console.log(req.url, MOCK_URL, req.url === MOCK_URL);
    return true;
  });
}
