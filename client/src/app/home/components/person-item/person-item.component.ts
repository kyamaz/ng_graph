import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { AppPerson } from '@models/app.model';
import { DataState } from '@appServices/data-machine.service';

@Component({
  selector: 'person-item',
  templateUrl: './person-item.component.html',
  styleUrls: ['./person-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PersonItemComponent implements OnInit {
  @Input() datum: Omit<AppPerson, 'phoneNumber'>;
  @Input() state: DataState;
  constructor() {}

  ngOnInit(): void {}
}
