import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InputErrorComponent } from './../input-error/input-error.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SigninComponent } from './signin.component';
import { By } from '@angular/platform-browser';
import { AuthService } from '@appServices/auth.service';

const FIXTURE_LOGIN = {
  username: {
    valid: 'toto@toto.fr',
    invalid: 'invalid'
  },
  password: {
    valid: 'mockmockmock',
    invalid: 'invalid'
  }
};

class MockAuthService extends AuthService {
  authenticated = false;

  mockAuthenticate(user) {
    const isValid =
      user.username === FIXTURE_LOGIN.username.valid &&
      user.password === FIXTURE_LOGIN.password.valid;
    this.authenticated = isValid;
    if (this.authenticated) {
      localStorage.setItem('access_token', '12345');
    }

    return this.authenticated;
  }

  mockIsAuthenticated() {
    return !!localStorage.getItem('access_token');
  }
}

describe('SigninComponent', () => {
  let component: SigninComponent;
  let fixture: ComponentFixture<SigninComponent>;
  let formEl;
  let submitBtnEl;
  let errMsg: HTMLSpanElement;
  let t;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatButtonModule,
        MatSelectModule,
        MatProgressSpinnerModule,
        MatIconModule,
        HttpClientTestingModule,
        RouterTestingModule,
        BrowserAnimationsModule
      ],
      declarations: [SigninComponent, InputErrorComponent],
      providers: [{ provide: MockAuthService, useClass: MockAuthService }]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(SigninComponent);
        component = fixture.componentInstance;
        formEl = fixture.debugElement.query(By.css('form')).nativeElement;
        submitBtnEl = fixture.debugElement.query(By.css('button'))
          .nativeElement;

        spyOn(MockAuthService.prototype, 'authenticate');
      });
  }));

  afterEach(() => {
    localStorage.removeItem('access_token');
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should disable submit button', () => {
    fixture.detectChanges();
    spyOn(component, 'onSubmit');
    submitBtnEl.click();
    expect(component.onSubmit).toHaveBeenCalledTimes(0);
  });

  it('should invalidate form', async(() => {
    component.form.get('username').setValue(FIXTURE_LOGIN.username.invalid);
    component.form.get('password').setValue(FIXTURE_LOGIN.password.invalid);
    expect(component.form.valid).toBeFalsy();
  }));

  it('should invalidate pwd', async(() => {
    component.form.get('password').setValue(FIXTURE_LOGIN.password.invalid);
    expect(component.form.get('password').valid).toBeFalsy();
  }));
  it('should display message on invalidate pwd ', async(() => {
    component.form.get('password').setValue(FIXTURE_LOGIN.password.invalid);
    errMsg = fixture.debugElement.query(By.css('[data-pwd_err]')).nativeElement;
    expect(errMsg).toBeDefined();
  }));

  it('should invalidate username', async(() => {
    component.form.get('username').setValue(FIXTURE_LOGIN.username.invalid);
    expect(component.form.get('username').valid).toBeFalsy();
  }));

  it('should display message on invalidate user ', async(() => {
    component.form.get('username').setValue(FIXTURE_LOGIN.username.invalid);
    errMsg = fixture.debugElement.query(By.css('[data-user_err]'))
      .nativeElement;
    expect(errMsg).toBeDefined();
  }));

  it('should validate username', async(() => {
    component.form.get('username').setValue(FIXTURE_LOGIN.username.valid);
    expect(component.form.get('username').valid).toBeTruthy();
  }));
  it('should validate password', async(() => {
    component.form.get('password').setValue(FIXTURE_LOGIN.password.valid);
    expect(component.form.get('password').valid).toBeTruthy();
  }));
  it('should validate form', async(() => {
    component.form.get('username').setValue(FIXTURE_LOGIN.username.valid);
    component.form.get('password').setValue(FIXTURE_LOGIN.password.valid);
    expect(component.form.valid).toBeTruthy();
  }));

  it('should invalidate credentials', async(() => {
    component.form.get('username').setValue(FIXTURE_LOGIN.username.invalid);
    component.form.get('password').setValue(FIXTURE_LOGIN.password.valid);
    MockAuthService.prototype.mockAuthenticate(component.form.value);
    expect(MockAuthService.prototype.mockIsAuthenticated()).toBeFalsy();
  }));

  /*   it('should display message in invalidate credentials', () => {
    // component.failed$.next(true);
    component.failed$.next(true);
    component.failed$
      .pipe(
        tap(f => {
          fixture.detectChanges();

          console.log(f, t);
        })
      )
      .subscribe();
    //expect(errMsg).toBeDefined();
  }); */

  it('should validate credentials', async(() => {
    component.form.get('username').setValue(FIXTURE_LOGIN.username.valid);
    component.form.get('password').setValue(FIXTURE_LOGIN.password.valid);
    MockAuthService.prototype.mockAuthenticate(component.form.value);
    expect(MockAuthService.prototype.mockIsAuthenticated()).toBeTruthy();
  }));
});
