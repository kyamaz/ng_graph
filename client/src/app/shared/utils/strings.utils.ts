export function normalizeStrToCompare(str: string): string {
  return str
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '') //replace any accented chara
    .replace(' ', '_')
    .toLowerCase();
}

export function firstLetter(s: string): string {
  if (typeof s !== 'string') return '';
  return s.charAt(0);
}

export class StringsUtils {
  //constructor() {}
  static getFromBetween = {
    results: [],
    string: '',
    getFromBetween: function(sub1, sub2) {
      if (this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0)
        return false;
      var SP = this.string.indexOf(sub1) + sub1.length;
      var string1 = this.string.substr(0, SP);
      var string2 = this.string.substr(SP);
      var TP = string1.length + string2.indexOf(sub2);
      return this.string.substring(SP, TP);
    },
    removeFromBetween: function(sub1, sub2) {
      if (this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0)
        return false;
      var removal = sub1 + this.getFromBetween(sub1, sub2) + sub2;
      this.string = this.string.replace(removal, '');
    },
    getAllResults: function(sub1, sub2) {
      // first check to see if we do have both substrings
      if (this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0)
        return;

      // find one result
      var result = this.getFromBetween(sub1, sub2);
      // push it to the results array
      this.results.push(result);
      // remove the most recently found one from the string
      this.removeFromBetween(sub1, sub2);

      // if there's more substrings
      if (this.string.indexOf(sub1) > -1 && this.string.indexOf(sub2) > -1) {
        this.getAllResults(sub1, sub2);
      } else return;
    },
    getValues: function(string, sub1, sub2) {
      if (!string) {
        return;
      }
      this.results = [];
      this.string = string;
      this.getAllResults(sub1, sub2);
      return this.results;
    }
  };

  /**
   * Adapted from: http://stackoverflow.com/questions/650022/how-do-i-split-a-string-with-multiple-separators-in-javascript
   */

  /**
   * Flatten a list of strings
   * http://rosettacode.org/wiki/Flatten_a_list
   */
  static flatten(arr) {
    var self = this;
    return arr.reduce(function(acc, val) {
      return acc.concat(val.constructor === Array ? self.flatten(val) : val);
    }, []);
  }

  /**
   * Recursively Traverse a list and apply a function to each item
   * @param list array
   * @param expression Expression to use in func
   * @param func function of (item,expression) to apply expression to item
   *
   */
  static traverseListFunc(list, expression, index, func) {
    var self = this;
    if (list[index]) {
      if (list.constructor !== String && list[index].constructor === String) {
        if (list[index] != func(list[index], expression)) {
          list[index] = func(list[index], expression);
        }
        if (list[index].constructor === Array) {
          self.traverseListFunc(list[index], expression, 0, func);
        }
        if (list.constructor === Array) {
          self.traverseListFunc(list, expression, index + 1, func);
        }
      }
      /*         list[index] != func(list[index], expression) ? (list[index] = func(list[index], expression)) : null;
      list[index].constructor === Array ? self.traverseListFunc(list[index], expression, 0, func) : null;
      list.constructor === Array ? self.traverseListFunc(list, expression, index + 1, func) : null; */
    }
  }

  /**
   * Recursively map function to string
   * @param string
   * @param expression Expression to apply to func
   * @param function of (item, expressions[i])
   */
  static mapFuncToString(string, expressions, func) {
    var list = [string];
    for (var i = 0, len = expressions.length; i < len; i++) {
      this.traverseListFunc(list, expressions[i], 0, func);
    }
    return this.flatten(list);
  }

  /**
   * Split a string
   * @param splitters Array of characters to apply the split
   */
  static splitString(string, splitters) {
    return this.mapFuncToString(string, splitters, function(item, expression) {
      return item.split(expression);
    });
  }
}

export function normaliseKey(str: string): string {
  return str.trim().toLowerCase();
}

/* function _getTranslation(keysMap: { [key: string]: string }, key: string): string {
  // return  keysMap.hasOwnProperty(key) ? keysMap[key] : "";//key are not formatted
  const val = Object.keys(keysMap).find(k => normaliseKey(k) === key);
  if (!!val) {
    return val;
  }
  return "";
} */
/* export function translateVariables(
  keys: Array<string> = [],
  translationMap: { [key: string]: string },
  content: string = ""
): string {
  return keys.reduce(
    (acc, curr) =>
      acc.replace(
        normaliseKey(`{${curr}}`), //normalise the str par in txt. should be { VARIABLE }
        _getTranslation(translationMap, normaliseKey(curr)) // replace { VARIABLE } with variable value
      ),
    content
  );
}

export function hasInvalidTranslateVariables(
  keys: Array<string> = [],
  translationMap: { [key: string]: string }
): boolean {
  return keys.reduce((acc, curr) => {
    return acc && !!_getTranslation(translationMap, normaliseKey(curr));
  }, true);
} */
