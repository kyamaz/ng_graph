import { HomeModule } from './../../../home/home.module';
import { LoginComponent } from './../../../login/login/login.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick
} from '@angular/core/testing';

import { AppNavComponent, setNavGroup } from './app-nav.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { APP_ROUTES } from '@routes/app-routing.module';
import { By } from '@angular/platform-browser';
import { NgModuleFactoryLoader, NO_ERRORS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ProtectedPageModule } from 'src/app/protected-page/protected-page.module';

const NAV_LINKS = [
  {
    value: `/app/`,
    viewValue: 'home',
    icon: 'pageview',
    msg: 'home page'
  }
];

describe('AppNavComponent', () => {
  let component: AppNavComponent;
  let fixture: ComponentFixture<AppNavComponent>;
  let nav: HTMLAnchorElement;
  let router: Router;
  let location: Location;
  let loader;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [
        RouterTestingModule.withRoutes(APP_ROUTES),
        ProtectedPageModule,
        HttpClientTestingModule,
        MatListModule,
        MatIconModule
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppNavComponent);
    component = fixture.componentInstance;
    loader = TestBed.get(NgModuleFactoryLoader);
    component.user_id = 0;

    component = fixture.componentInstance;
    component.navItems = NAV_LINKS;

    router = TestBed.get(Router);
    location = TestBed.get(Location);
    spyOn(component, 'ngOnInit');

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('on init, it should generate nav links ', () => {
    component.navItems = setNavGroup(0);
    fixture.detectChanges();

    expect(component.navItems).toEqual(NAV_LINKS);
  });

  it('should navigate to home page', fakeAsync(() => {
    loader.stubbedModules = { lazyModule: HomeModule };
    loader.stubbedModules = {
      './../../../home/home.module#HomeModule': HomeModule
    };
    router.resetConfig([
      {
        path: 'app',
        loadChildren: () =>
          import('./../../../home/home.module').then(m => m.HomeModule)
      }
    ]);

    nav = fixture.debugElement.query(By.css('#home')).nativeElement;
    nav.dispatchEvent(
      new MouseEvent('click', {
        view: window,
        bubbles: true,
        cancelable: true
      })
    );

    fixture.detectChanges();
    tick();

    expect(location.path()).toBe('/app');
  }));
});
