import { AuthPayload, CONST, USER_AUTH } from './app.model';
import { Injectable } from '@nestjs/common';
import { MD5 } from './utils';
import { of, throwError } from 'rxjs';

@Injectable()
export class AppService {
  signin({ email, password }: AuthPayload): any {
    const hash = MD5(`${password}&${email}`);
    if (CONST.expectedHash === hash) {
      return of({ ...USER_AUTH, access_token: hash });
    }
    return throwError('invalid credentials');
  }

  refresh() {
    return of({ ...USER_AUTH, access_token: CONST.expectedHash });
  }
}
