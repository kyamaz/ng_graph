import { MOCK_USER_SERVICE_RESPONSE } from './../fixtures/service';
/* import { SetAppUSerAction } from './../store/app.actions';
import { AppState } from './../store/app.state'; */
import { HttpClient } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';

import { CanActivateUserService } from './can-activate-user.service';
import { RouterTestingModule } from '@angular/router/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { MOCK_GENERIC_SERVICE_RESPONSE } from '../fixtures/service';
import { Router } from '@angular/router';

describe('CanActivateUserService', () => {
  let router: Router;
  let navigateSpy;
  let mockLocalStorage = null;
  let store = {};
  beforeEach(() => {
    mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      },
      removeItem: (key: string) => {
        delete store[key];
      },
      clear: () => {
        store = {};
      }
    };

    spyOn(localStorage, 'getItem').and.callFake(mockLocalStorage.getItem);
    spyOn(localStorage, 'setItem').and.callFake(mockLocalStorage.setItem);
    spyOn(localStorage, 'removeItem').and.callFake(mockLocalStorage.removeItem);
    spyOn(localStorage, 'clear').and.callFake(mockLocalStorage.clear);

    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],

      providers: [
        {
          provide: CanActivateUserService,
          useValue: MOCK_GENERIC_SERVICE_RESPONSE
        }
      ]
    });
    router = TestBed.get(Router);
    navigateSpy = spyOn(router, 'navigate');
  });

  it('should be created', () => {
    const service: CanActivateUserService = TestBed.get(CanActivateUserService);
    expect(service).toBeTruthy();
  });

  it('allow admin user', inject(
    [HttpClient, HttpTestingController],
    (http: HttpClient, httpMock: HttpTestingController) => {
      //make sure the service is called
      //   store.dispatch(new SetAppUSerAction(MOCK_INIT_ADMIN_STORE))

      localStorage.setItem(
        'naister_user',
        JSON.stringify(MOCK_USER_SERVICE_RESPONSE.resp)
      );
      router.navigate(['/']);

      /* store
        .selectOnce(state => state.app.user)
        .subscribe(user => {
          expect(user.is_admin).toBeTruthy();
        }); */

      expect(navigateSpy).toHaveBeenCalledWith(['/']);

      httpMock.verify();
    }
  ));
  /* it('reject non admin user', inject(
    [HttpClient, HttpTestingController],
    (http: HttpClient, httpMock: HttpTestingController) => {
      //make sure the service is called
      store.dispatch(new SetAppUSerAction(MOCK_USER_STORE));

      router.navigate(['/', 'profil']);

      store
        .selectOnce(state => state.app.user)
        .subscribe(user => {
          expect(user.is_admin).toBeFalsy();
        });
      expect(navigateSpy).toHaveBeenCalledWith(['/', 'profil']);
      httpMock.verify();
    }
  )); */
});
