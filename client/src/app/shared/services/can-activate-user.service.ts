import { AppService } from './app.service';
import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivateChild,
  Router,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CanActivateUserService implements CanActivateChild {
  constructor(private _router: Router, private _appService: AppService) {}

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    console.log('TODO CanActivateChild add user');

    const isAdmin = this._appService.userResponse.is_admin;
    if (isAdmin) {
      return true;
    }
    return true;
  }
}
