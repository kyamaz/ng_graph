import { Injectable } from '@angular/core';
import { LOCAL_STORAGE_KEY } from '../const';
import { BaseUser, UserGraph } from '@models/user.model';
import { map, tap } from 'rxjs/operators';
import { Observable, of, ReplaySubject } from 'rxjs';
import { Router } from '@angular/router';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { AppPerson } from '@models/app.model';
import { SortKey, SortOrder, SortState } from '@models/index';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  constructor(private _router: Router, private _apollo: Apollo) {}

  private _appUser: BaseUser;
  public graphUser$: ReplaySubject<Partial<UserGraph>> = new ReplaySubject(1);
  public persons: Omit<AppPerson, 'phoneNumber'>[] = [];
  public context$: ReplaySubject<SortState> = new ReplaySubject(1);

  get userResponse(): BaseUser {
    // ugly
    const hasCache = !!localStorage.getItem(LOCAL_STORAGE_KEY);
    if (hasCache) {
      return JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
    }
    return this._appUser;
  }

  set userResponse(user: BaseUser) {
    this._appUser = user;
  }

  logOut(): Observable<Promise<boolean>> {
    return of(true).pipe(
      tap((_) => {
        this.userResponse = null;
        localStorage.removeItem(LOCAL_STORAGE_KEY);
      }),
      map((_) => this._router.navigate(['/', 'login']))
    );
  }

  getUser(fields: string): Observable<Partial<UserGraph>> {
    return this._apollo
      .watchQuery({
        query: gql`
          query user($id: String!) {
            user(id: $id) {
              ${fields}
            }
          }
        `,
        variables: {
          id: this.userResponse.id,
        },
      })
      .valueChanges.pipe(
        map((user) => (user.data as { user: Partial<UserGraph> }).user),
        tap((u) => this.graphUser$.next(u))
      );
  }

  updateUser(user: Partial<UserGraph>) {
    return this._apollo
      .mutate({
        mutation: gql`
          mutation updateUser($id: String!, $appUser: UpdateAppUser!) {
            updateUser(id: $id, appUser: $appUser) {
              userName
              firstName
              lastName
              avatar
            }
          }
        `,
        variables: {
          id: this.userResponse.id,
          appUser: user,
        },
      })
      .pipe(
        map(
          (userGraph) =>
            (userGraph.data as { updateUser: Partial<UserGraph> }).updateUser
        ),
        tap((u) => this.graphUser$.next(u))
      );
  }

  getPersons(
    index: number = 0,
    order: SortOrder = 'asc',
    sort: SortKey = '_id'
  ): Observable<Omit<AppPerson, 'phoneNumber'>[]> {
    this.context$.next({ key: sort, order });
    return this._apollo
      .watchQuery({
        query: gql`
          query persons {
            persons(chunk: ${index}, limit: 20, order:"${order}", sort:"${sort}" ) {
              id
              avatar
              firstName
              lastName
              jobTitle
              jobDescription
              company {
                name
                logo
                id
                people {
                  id
                  firstName
                  lastName
                  jobTitle
                }
              }
            }
          }
        `,
      })
      .valueChanges.pipe(
        map((persons) => (persons.data as { persons: Array<any> }).persons)
      );
  }

  searchInName(query: string): Observable<any> {
    return this._apollo
      .watchQuery({
        query: gql`
          query searchPersons {
            searchPersons(query: "${query}") {
              id
              firstName
              lastName
            }
          }
        `,
      })
      .valueChanges.pipe(
        map(
          (result) =>
            (result.data as {
              searchPersons: Pick<AppPerson, 'id' | 'firstName' | 'lastName'>;
            }).searchPersons
        )
      );
  }
}
