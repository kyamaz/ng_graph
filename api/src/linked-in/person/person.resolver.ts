import {
  Args,
  Query,
  Resolver,
  ResolveField,
  Parent,
  ResolveProperty,
  Int,
} from '@nestjs/graphql';
import { PersonType } from './person.dto';
import { LinkedInService } from './../linked-in.service';
import { Person, ViewPerson } from './person.interface';
import { Company } from '../company/company.interface';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from '../../auth.guard';

@Resolver((of) => PersonType)
export class PersonResolver {
  constructor(private _linkedInService: LinkedInService) {}

  @Query(() => [PersonType], { name: 'persons' })
  @UseGuards(AuthGuard)
  async persons(
    @Args('chunk', { type: () => Int }) chunk: number,
    @Args('limit', { type: () => Int }) limit: number,
    @Args('order', { type: () => String }) order: string,
    @Args('sort', { type: () => String }) sort: string,
  ): Promise<ViewPerson[]> {
    return await this._linkedInService.getPersonList(chunk, limit, order, sort);
  }
  @Query(() => [PersonType], { name: 'searchPersons' })
  @UseGuards(AuthGuard)
  async searchPersons(
    @Args('query', { type: () => String }) query: string,
  ): Promise<ViewPerson[]> {
    return await this._linkedInService.queryPerson(query);
  }

  @Query(() => PersonType)
  @UseGuards(AuthGuard)
  async person(@Args('id') id: string): Promise<ViewPerson> {
    return await this._linkedInService.getPerson(id);
  }

  // resolved field
  @ResolveField()
  async company(@Parent() person: Person): Promise<Company> {
    // This will be a database call, just mocking data for now.
    if (!person.companyId) {
      console.log('Resolver auth in recipe', person);
    }
    return this._linkedInService.getCompany(person.companyId);
  }
}
