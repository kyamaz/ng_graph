# graph_ng

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.5.

## gitlab ci

pb install ng test on gitlab ci
https://github.com/karma-runner/karma-chrome-launcher/issues/158

---

## Deployement

# When merge request dev -> staging, be sure to commit the staging context. automated process will apply it.

# switch back the dev context by pushing it

## Sonar

### pb install sonar

java not install while java -version works
update wraper conf with right path
wrapper.java.command= /Library/Java/JavaVirtualMachines/jdk-12.0.2.jdk/Contents/Home/bin/java
https://community.sonarsource.com/t/sonerqube-doesnt-start/11507/3

---

## Docker

- install docker

- create Dockerfile

this project is a simple container with a node server.  
 _ Note: the file name must be capitalized for Aws. dockerfiler won't be recognized _

- build image
  run in the terminal :
  `docker build --rm -t naister_prod .`  
   \_ Note: . at the end is mandatory  
  the tag is optional. much easier to identify. here naister_prod

- run localy
  run in the terminal :
  `docker run -p 3000:8080 naister_prod`
  open localhost:3000 / `curl -i localhost:3000` to test
  or using thhis ngnix conf  
  `docker run -p 80:80 naister_prod`  
   open localhost/ `curl -i localhost` to test  
   _ Note: bind the docker container port with custom one (3000 in this example) p stands for port _

- useful docker command  
  `docker ps` list container  
  `docker kill :id` kill id. Often the three first character is enough
  `docker login` ..to login
  `docker exec -it <container id> /bin/bash` to enter in the container

- deploy on aws using eb cli
  run in the terminal :
  `eb deploy`  
   _ Note _ :

* _ you should be logged to your aws application. To be sure type `eb init -i`(force initialisation eb cli).  
  Be sure to select the right region. the one hosting the app. It should appear in the select application menu. _
* _ the docker image is local. so you don't need dockerrun.aws.jon. It 's needed to provide credentails to remote repo _

_ `eb init -i --profile profile-name` to init eb with another account _

---

## Manual deployement

- build artefact
  `ng build --prod`  
   _ Note: At this stage, you should have merged with dev or staging, to pull latest changes_

- build docker image
  run in the terminal :
  `docker build --rm -t [naister_prod|| naister_staging (depending tour current branch)] .`

- run docker localy
  `docker run -p 80:80 [naister_prod|| naister_staging (depending tour current branch)]`  
   open localhost/ `curl -i localhost` to test

- run eb localy
  `eb local run --port 80`

- run eb deploy  
   _ Note: make sur you arre in the right. eb status.  
   if necessary, run eb use [dlp-prod|| staging (depending tour current branch)] _

- check deployment
  `eb open`

_ Note: shotcut shell script. Careful not tested and and manual validation.
`sh ./prod-gitlab-deploy.sh`_

---

## Angular

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
