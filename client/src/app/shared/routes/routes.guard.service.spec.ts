import { AuthService } from './../services/auth.service';
/* import { SetAppUSerAction } from './../store/app.actions';
import { AppState } from './../store/app.state'; */
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TestBed } from '@angular/core/testing';

import { RoutesGuardService } from './routes.guard.service';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { MOCK_USER_SERVICE_RESPONSE } from '../fixtures/service';

describe('RoutesGuardService', () => {
  let service: RoutesGuardService;
  let router: Router;
  let navigateSpy;
  let mockLocalStorage = null;
  let store = {};

  beforeEach(() => {
    mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      }
    };

    spyOn(localStorage, 'getItem').and.callFake(mockLocalStorage.getItem);
    spyOn(localStorage, 'setItem').and.callFake(mockLocalStorage.setItem);

    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [
        RoutesGuardService,
        {
          provide: AuthService,
          useValue: {
            checkAuthentication() {
              return of(true);
            }
          }
        }
      ]
    });
    service = TestBed.get(RoutesGuardService);
    router = TestBed.get(Router);
    navigateSpy = spyOn(router, 'navigate');
  });

  it('should be created', () => {
    const service: RoutesGuardService = TestBed.get(RoutesGuardService);
    expect(service).toBeTruthy();
  });

  it('should be deactivated', () => {
    localStorage.setItem(
      'naister_user',
      JSON.stringify({
        access_token: null,
        user_name: 'null',
        user_id: null,
        is_admin: false,
        business_id: null,
        access_token_type: null,
        expires: null
      })
    );

    service.canActivate().subscribe(res => expect(res).toBeFalsy());
  });
  it('should be activated on user token', () => {
    localStorage.setItem(
      'naister_user',
      JSON.stringify(MOCK_USER_SERVICE_RESPONSE.resp)
    );

    service.canActivate().subscribe(res => {
      expect(res).toBeTruthy();
    });
  });
});
