import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { AppUser } from './app-user.interface';
import { Model } from 'mongoose';
import { AppUserType } from './app-user.dto';
import { UpdateAppUser } from './update-app-user';
import { AddAppUser } from './add-app-user';
import { MD5 } from '../utils';

const hashCode = (s) =>
  // tslint:disable-next-line:no-bitwise
  Math.abs(
    s.split('').reduce((a, b) => ((a << 5) - a + b.charCodeAt(0)) | 0, 0),
  );

@Injectable()
export class AppUserService {
  constructor(@InjectModel('AppUser') private _appUserModel: Model<AppUser>) {
    // this._appUserModel.deleteMany(() => true);
  }

  async createAppUser(createAppUser: AddAppUser): Promise<AppUserType> {
    const _appUser = {
      ...createAppUser,
      password: MD5(createAppUser.password),
    };

    const newAppUser = new this._appUserModel(_appUser);
    newAppUser.id = newAppUser._id;
    return newAppUser.save();
  }

  async getAllAppUser() {
    return await this._appUserModel.find().exec();
  }

  async getAppUser(id: string): Promise<AppUserType> {
    return await this._appUserModel.findOne({ id });
  }

  async delete(id: string): Promise<UpdateAppUser> {
    return await this._appUserModel.findOneAndDelete({ id });
  }

  async update(id: string, appUser: UpdateAppUser): Promise<AppUserType> {
    return await this._appUserModel.findOneAndUpdate({ id }, appUser, {
      new: true,
    });
  }

  async getRegistredUser(
    email: string,
    password: string,
  ): Promise<AppUserType> {
    return this._appUserModel.findOne({
      email,
      password: MD5(password),
    });
  }

  async getToken(token: string): Promise<boolean> {
    return await this._appUserModel.find({}).then((res) => {
      return res.some((user) => MD5(user.email) === token);
    });
  }
}
