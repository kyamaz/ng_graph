import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoutesGuardService } from './routes.guard.service';

export const DEFAULT_REDIRECT = '/app/not-found';
export const APP_ROUTES: Routes = [
  {
    path: 'login',
    loadChildren: () =>
      import('./../../login/login.module').then((m) => m.LoginModule),
  },
  {
    path: 'app',
    canActivate: [RoutesGuardService],
    loadChildren: () =>
      import('./../../protected-page/protected-page.module').then(
        (m) => m.ProtectedPageModule
      ),
  },
  { path: '**', redirectTo: DEFAULT_REDIRECT },
];

@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
