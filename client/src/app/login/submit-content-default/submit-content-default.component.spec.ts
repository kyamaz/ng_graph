import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitContentDefaultComponent } from './submit-content-default.component';

describe('SubmitContentDefaultComponent', () => {
  let component: SubmitContentDefaultComponent;
  let fixture: ComponentFixture<SubmitContentDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SubmitContentDefaultComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitContentDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
