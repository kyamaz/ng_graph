import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitContentErrorComponent } from './submit-content-error.component';

describe('SubmitContentErrorComponent', () => {
  let component: SubmitContentErrorComponent;
  let fixture: ComponentFixture<SubmitContentErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SubmitContentErrorComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitContentErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
