import { UserResponse } from '@models/index';
import { RouterTestingModule } from '@angular/router/testing';
import { inject, TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { Router } from '@angular/router';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { HttpInterceptService } from './http.intercept.service';
import { MOCK_CREDS, MOCK_USER_SERVICE_RESPONSE } from '../fixtures/service';
import { API_ROOT } from '../const';

describe('AuthService', () => {
  let router: Router;
  let navigateSpy;
  let service: AuthService;
  let store = {};
  let mockLocalStorage = null;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [
        {
          provide: AuthService,
          useValue: MOCK_USER_SERVICE_RESPONSE
        },

        {
          provide: HTTP_INTERCEPTORS,
          useClass: HttpInterceptService,
          multi: true
        }
      ]
    });
    service = TestBed.get(AuthService);

    router = TestBed.get(Router);
    navigateSpy = spyOn(router, 'navigate');
    mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      },
      removeItem: (key: string) => {
        delete store[key];
      },
      clear: () => {
        store = {};
      }
    };
    spyOn(localStorage, 'getItem').and.callFake(mockLocalStorage.getItem);
    spyOn(localStorage, 'setItem').and.callFake(mockLocalStorage.setItem);
    spyOn(localStorage, 'removeItem').and.callFake(mockLocalStorage.removeItem);
    spyOn(localStorage, 'clear').and.callFake(mockLocalStorage.clear);
  });
  afterEach(inject(
    [HttpTestingController],
    (backend: HttpTestingController) => {
      backend.verify();
    }
  ));
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('add user to store on success', inject(
    [HttpClient, HttpTestingController],
    (http: HttpClient, httpMock: HttpTestingController) => {
      //make sure the service is called
      http
        .post(`${API_ROOT}api/account/token/`, MOCK_CREDS)
        .subscribe((response: any) => {
          localStorage.setItem('naister_user', JSON.stringify(response.resp));
          expect(response).toBeTruthy();
        });

      const req: any = httpMock.expectOne(`${API_ROOT}api/account/token/`);
      expect(req.request.method).toEqual('POST');
      req.flush(MOCK_USER_SERVICE_RESPONSE, {
        status: 200,
        statusText: 'success'
      });
      const user: UserResponse = JSON.parse(
        localStorage.getItem('naister_user')
      );
      expect(user.access_token).toBeTruthy();

      httpMock.verify();
    }
  ));

  it('navigate to page on success', inject(
    [HttpClient, HttpTestingController],
    (http: HttpClient, httpMock: HttpTestingController) => {
      //make sure the service is called
      http
        .post(`${API_ROOT}api/account/token/`, MOCK_CREDS)
        .subscribe((response: any) => {
          localStorage.setItem('naister_user', JSON.stringify(response.resp));

          router.navigate(['/', 'app', 'project']);
          expect(response).toBeTruthy();
        });

      const req: any = httpMock.expectOne(`${API_ROOT}api/account/token/`);
      req.flush(MOCK_USER_SERVICE_RESPONSE, {
        status: 200,
        statusText: 'success'
      });
      expect(req.request.method).toEqual('POST');
      expect(navigateSpy).toHaveBeenCalledWith(['/', 'app', 'project']);
      httpMock.verify();
    }
  ));
  it('clear store ', () => {
    localStorage.removeItem('naister_user');
    expect(localStorage.getItem('naister_user')).toBeFalsy();
  });
  /*   it('ignore user in store on failure', inject(
    [HttpClient, HttpTestingController],
    (http: HttpClient, httpMock: HttpTestingController) => {
      //make sure the service is called
      http.post(`${API_ROOT}api/account/token/`, MOCK_CREDS).subscribe((response: any) => {
        store.dispatch(new SetAppUSerAction(response.resp));
        expect(response.tok).toBeTruthy();
      });

      const req: any = httpMock.expectOne(`${API_ROOT}api/account/token/`);
      expect(req.request.method).toEqual('POST');
      req.flush(null, { status: 401, statusText: 'unauthorized' });
      store
        .selectOnce(state => state.app.user)
        .subscribe(user => {
          expect(user.token).toBeFalsy();
        });

      httpMock.verify();
    }
  )); */
});
