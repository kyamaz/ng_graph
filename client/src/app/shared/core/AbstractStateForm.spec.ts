import { FormsService } from './../deps/customForms/forms.services';
import { AbstractStateForm } from './AbstractStateForm';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { tap } from 'rxjs/operators';

const MOCK_SIMPLE_CHANGES = {
  data: {
    currentValue: {
      mock: 'mock'
    }
  }
};
const MOCK_STATE_CHANGES = {
  stateBtn: {
    currentValue: 'SUCCESS',
    previousValue: 'INITIAL'
  }
};
const MOCK_STATE_HNO_CHANGES = {
  stateBtn: {
    currentValue: 'SUCCESS',
    previousValue: 'SUCCESS'
  }
};

class MOCK_FORM_STATE extends AbstractStateForm<any> {
  constructor(protected _formService: FormsService<any>) {
    super(_formService);
  }

  setFormValidation() {}
}

describe('Abstract state form', () => {
  let formService: FormsService<any>;
  let abstractFormState;
  let mockForm;

  beforeEach(() => {
    formService = new FormsService();
    abstractFormState = new MOCK_FORM_STATE(formService);
    mockForm = new FormGroup({
      data: new FormControl('mock', Validators.required)
    });
  });

  it('should build form on simple changes', () => {
    const res = abstractFormState.initStateForm(MOCK_SIMPLE_CHANGES, 'data');
    expect(res instanceof FormGroup).toBeTruthy();
  });

  it('should update validate state on changes', () => {
    abstractFormState.subFormChanges(mockForm).subscribe();
    mockForm.get('data').setValue('update');
    abstractFormState.formValidState$
      .pipe(tap(status => expect(status).toEqual('VALID')))
      .subscribe();
  });
  it('should update invalidate state on invalid input', () => {
    mockForm.get('data').setValue(null);

    abstractFormState.formValidState$
      .pipe(tap(status => expect(status).toEqual('INVALID')))
      .subscribe();
  });
  it('should update state upon inputs', () => {
    abstractFormState.updateState(MOCK_STATE_CHANGES);
    abstractFormState.btnState$
      .pipe(tap(status => expect(status).toEqual('SUCCESS')))
      .subscribe();
  });

  it('should not  update state upon same state', () => {
    let res = abstractFormState.updateState(MOCK_STATE_CHANGES);
    expect(res).toBeUndefined();
  });

  it('should not  update state upon invalid state', () => {
    let res = abstractFormState.updateState({ invalid: false });
    expect(res).toBeUndefined();
  });
});
