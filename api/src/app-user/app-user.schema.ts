import * as mongoose from 'mongoose';

export const AppUserSchema = new mongoose.Schema({
  id: String,
  userName: String,
  email: String,
  password: String,
  firstName: String,
  lastName: String,
  avatar: String,
});
