import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { AppPerson } from '@models/app.model';
import { DataState } from '@appServices/data-machine.service';

@Component({
  selector: 'app-row',
  templateUrl: './row.component.html',
  styleUrls: ['./row.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RowComponent implements OnInit {
  @Input() state: DataState;
  @Input() row: Omit<AppPerson, 'phoneNumber'>[];
  constructor() {}

  ngOnInit(): void {}
}
