import { Field, ID, ObjectType } from '@nestjs/graphql';
import { CompanyType } from '../company/company.dto';

@ObjectType()
export class PersonType {
  @Field(() => ID, { description: 'person id' })
  readonly id: string;
  @Field(() => String, {
    description: 'first name',
  })
  firstName: string;
  @Field(() => String, {
    description: 'last name',
  })
  lastName: string;
  @Field(() => String, { description: 'person avatar', nullable: true })
  avatar: string;

  @Field(() => String, { description: 'job title', nullable: true })
  jobTitle: string;

  @Field(() => String, { description: 'job description', nullable: true })
  jobDescription: string;
  @Field(() => String, { description: 'phone', nullable: true })
  phoneNumber: string;

  @Field((type) => CompanyType, { description: 'company', nullable: true })
  company: CompanyType;
}
