import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { MD5 } from './utils';
import { CONST } from './app.model';
import { GqlExecutionContext } from '@nestjs/graphql';
import { AppUserService } from './app-user/app-user.service';

function validateToken(tk: string): boolean {
  return MD5(tk) === CONST.expectedToken;
}

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private _appUserService: AppUserService) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    /*
    REST AUTH
    const request = context.switchToHttp().getRequest();
    const tk: InstanceType<any> = request.headers.authorization;
    return validateToken(tk);
    */
    const ctx = GqlExecutionContext.create(context);
    const request = ctx.getContext().request;
    const auth = request.get('Authorization');
    if (!auth) {
      return false;
    }
    const _tk = auth.split(' ');
    return this._appUserService.getToken(_tk[1]).then((response) => {
      return response;
    });
  }
}
