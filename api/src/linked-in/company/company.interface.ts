import { Document } from 'mongoose';
import { Person } from '../person/person.interface';

export interface BaseCompany extends Document {
  id: string;
  name: string;
  logo: string;
  phoneNumber: string;
}
export interface Company extends BaseCompany {
  personIds: string[];
}
export interface ViewCompany extends BaseCompany {
  people: Person[];
}
