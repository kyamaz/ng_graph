import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { ThemeService } from '@appServices/theme.service';

@Component({
  selector: 'theme-toggle',
  templateUrl: './theme-toggle.component.html',
  styleUrls: ['./theme-toggle.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThemeToggleComponent implements OnInit {
  public label: 'light' | 'dark' = 'light';

  constructor(private _themeService: ThemeService) {}

  ngOnInit(): void {
    this._themeService.toggleTheme(this.label);
  }

  handleToggle(ev: MatSlideToggleChange) {
    const { checked } = ev;
    this.label = checked ? 'dark' : 'light';
    this._themeService.toggleTheme(this.label);
  }
}
