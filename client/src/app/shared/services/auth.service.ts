import { AppService } from './app.service';
import { Router } from '@angular/router';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { Observable, of, ReplaySubject, throwError } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { WindowRef } from './../../win_ref';
import { catchError, filter, map, tap } from 'rxjs/operators';
import { environment } from 'environments/environment';
import { BaseUser } from '@models/user.model';
import { LOCAL_STORAGE_KEY } from '../const';

/*
 * DEPRECATED REST AUTH
 * */
interface AuthPayload {
  username: string;
  password: string;
}

const API_ROOT = environment.url;
@Injectable({
  providedIn: 'root',
})
// @deprecated
export class AuthService {
  public sessionToken$: ReplaySubject<string> = new ReplaySubject(1);
  public readonly url: string = this.winRef.getUrl();

  constructor(
    private http: HttpClient,
    private _router: Router,
    private winRef: WindowRef,
    private appService: AppService,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {}

  getToken(payload: { email: string; password: string }): Observable<BaseUser> {
    return this.http.post<any>(`${API_ROOT}api/account/token/`, payload);
  }

  refreshToken(access_token: string): Observable<BaseUser> {
    return this.http.post<any>(`${API_ROOT}api/account/refresh-token/`, {
      access_token,
    });
  }

  revokeToken(access_token: string): Observable<BaseUser> {
    return this.http.post<any>(`${API_ROOT}api/account/revoke-token/`, {
      access_token,
    });
  }

  authenticate(login: AuthPayload): Observable<void | Promise<boolean>> {
    const { username, password } = login;
    const payload = { email: username, password };
    return this.getToken(payload).pipe(
      map((log: BaseUser) => {
        this.appService.userResponse = log;

        localStorage.setItem('app_store', JSON.stringify(log));

        return this._router.navigate(['/', 'app']);
      }),

      catchError((e: any) => throwError(e))
    );
  }

  checkAuthentication(token: string): Observable<any> {
    //YOUR ENDPOINT HERE

    return this.refreshToken(token).pipe(
      map((log: BaseUser) => {
        this.appService.userResponse = log;
        return this._router.navigate(['/', 'app']);
      }),

      catchError((e: any) => throwError(e))
    );
  }

  clearAuthentication(token: string): Observable<void | Promise<boolean>> {
    return this.revokeToken(token).pipe(
      catchError((err: HttpErrorResponse) => {
        this.appService.userResponse = null;

        if (isPlatformBrowser(this.platformId)) {
          if (
            !!this.winRef.nativeWindow.localStorage.getItem(LOCAL_STORAGE_KEY)
          ) {
            this.winRef.nativeWindow.localStorage.removeItem(LOCAL_STORAGE_KEY);
          }
        }
        return of(false);
      }),

      filter((res) => !!res),
      map((res) => {
        this.appService.userResponse = null;
        if (isPlatformBrowser(this.platformId)) {
          if (
            !!this.winRef.nativeWindow.localStorage.getItem(LOCAL_STORAGE_KEY)
          ) {
            this.winRef.nativeWindow.localStorage.removeItem(LOCAL_STORAGE_KEY);
          }
        }
      }),
      tap((_) => this._router.navigate(['/', 'login']))
    );
  }
}
