import { filterList, getPaginationeType } from 'src/app/shared/utils';

const MOCK_NEXT_PAGE_EVENT = {
  pageIndex: 1,
  previousPageIndex: 0,
  pageSize: 15,
  length: 15
};

const MOCK_PREV_PAGE_EVENT = {
  pageIndex: 0,
  previousPageIndex: 1,
  pageSize: 15,
  length: 15
};

const MOCK_FULL_LIST = ['mock', 'test'];
const MOCK_SEL = ['mock'];
const MOCK_FILTERED = ['test'];
const MOCK_SEL_PREDICATE = [{ value: 'mock' }];

describe('arrays utils', () => {
  it('should paginate to previous page', () => {
    let result = getPaginationeType(MOCK_NEXT_PAGE_EVENT);
    expect(result).toBe('NEXT');
  });
  it('should paginate to previous page', () => {
    let result = getPaginationeType(MOCK_PREV_PAGE_EVENT);
    expect(result).toBe('PREVIOUS');
  });

  it('filter common value  without predicate function', () => {
    let result = filterList(MOCK_FULL_LIST, MOCK_SEL);
    expect(result).toEqual(MOCK_FILTERED);
  });
  it('filter common value  with predicate function', () => {
    let result = filterList(
      MOCK_FULL_LIST,
      MOCK_SEL_PREDICATE,
      (src, tkt) => src.value === tkt
    );
    expect(result).toEqual(MOCK_FILTERED);
  });
});
