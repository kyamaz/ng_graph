import { TestBed } from '@angular/core/testing';

import { SigninMachineService } from './signin-machine.service';

describe('SigninMachineService', () => {
  let service: SigninMachineService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SigninMachineService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
