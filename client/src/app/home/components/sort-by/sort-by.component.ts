import {
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { CdkOverlayOrigin, Overlay } from '@angular/cdk/overlay';
import { PopoverService } from '@appServices/popover.service';
import { SortListComponent } from '../sort-list/sort-list.component';
import { MatButton } from '@angular/material/button';

@Component({
  selector: 'sort-by',
  templateUrl: './sort-by.component.html',
  styleUrls: ['./sort-by.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SortByComponent implements OnInit, AfterViewInit {
  @ViewChild('sort_overlay') overlayTemplate: MatButton;
  private _key = 'sort';

  constructor(
    private overlay: Overlay,
    private _popoverService: PopoverService
  ) {}

  ngOnInit(): void {
    this._popoverService.setPositionStrategy('relative');
  }
  ngAfterViewInit() {
    const ref = { elementRef: this.overlayTemplate._elementRef };

    this._popoverService.setAttachComponent(SortListComponent, this._key);
    this._popoverService.setPortalOrigin(ref, this._key);
  }

  toggle() {
    this._popoverService.update(this._key);
  }
}
