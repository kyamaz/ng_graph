import { Injectable } from '@angular/core';
import { ComponentPortal, ComponentType } from '@angular/cdk/portal';
import { takeUntil, tap } from 'rxjs/operators';
import {
  CdkOverlayOrigin,
  Overlay,
  PositionStrategy,
} from '@angular/cdk/overlay';
import { Observable, of, Subject } from 'rxjs';

export type PositionStrategyType = 'global' | 'relative';

@Injectable({
  providedIn: 'root',
})
export class PopoverService {
  private _overlayRef: any;
  private _overlayOrigin: Record<string, CdkOverlayOrigin>;
  private _positionStrategy: PositionStrategyType;
  protected state: 'open' | 'close' = 'close';
  private _mapAttachedComponent: Record<string, ComponentType<any>>;

  constructor(protected overlay: Overlay) {}

  private dispose$: Subject<boolean> = new Subject<boolean>();

  /**
   * Component to display inside the portal
   * @param component
   */
  setAttachComponent(component, key) {
    this._mapAttachedComponent = {
      ...this._mapAttachedComponent,
      [key]: component,
    };
  }

  /**
   * Component to display inside the portal
   * @param strategy
   */
  setPositionStrategy(strategy: PositionStrategyType) {
    this._positionStrategy = strategy;
  }

  /**
   * for overlay flexibleConnectedTo/connect need to set the portal ref position
   * the portal x/y position will be based on this ref
   * @param origin
   */
  setPortalOrigin(origin: CdkOverlayOrigin, key) {
    this._overlayOrigin = {
      ...this._overlayOrigin,
      [key]: origin,
    };
  }

  /**
   * https://github.com/estellepicq/over-it
   *https://stackoverflow.com/questions/52432447/angular-cdk-understanding-the-overlay-position-system
   * basically two position strategy
   *  global (modal,...)
   *  relative (popover, tooltip,...)
   * @param position
   */
  applyPositionStrategy(type: PositionStrategyType, key): PositionStrategy {
    const orig = this._overlayOrigin[key];
    const POSITION_STRATEGY: { [key: string]: PositionStrategy } = {
      relative: this.overlay
        .position()
        .flexibleConnectedTo(orig.elementRef)
        .withPositions([
          {
            originX: 'center',
            originY: 'bottom',
            overlayX: 'center',
            overlayY: 'top',
          },
        ])
        .withPush(false),
    };
    return POSITION_STRATEGY[type];
  }

  close(): void {
    this._onClose();
  }

  update(key): void {
    if (this.state === 'open') {
      this._onClose();
      return;
    }
    this._onOpen(key);

    this._overlayRef
      .backdropClick()
      .pipe(
        tap((e) => this._onClose()),
        takeUntil(this.dispose$)
      )
      .subscribe(); // Allows to close overlay by clicking around it
  }

  /**
   * remove the portal
   * update state
   * there's also a detach method?
   * @private
   */
  private _onClose(): void {
    if (!!this._overlayRef) {
      this._overlayRef.dispose();
      this.state = 'close';
    }
  }

  /**
   * create the overlay then its content
   * update state
   * @private
   */
  private _onOpen(key): void {
    this._overlayRef = this.overlay.create({
      positionStrategy: this.applyPositionStrategy(this._positionStrategy, key),
      hasBackdrop: true,
      panelClass: ['position'],
      backdropClass: 'transparent',
    });
    const comp = this._mapAttachedComponent[key];
    const portalContent = new ComponentPortal(comp);
    this._overlayRef.attach(portalContent);
    this.state = 'open';
  }

  dispose(): Observable<'closed'> {
    this.close();
    this.dispose$.next(true);
    return of('closed');
  }
}
