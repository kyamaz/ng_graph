import {
  AfterViewChecked,
  ChangeDetectionStrategy,
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import { UserGraph } from '@models/user.model';
import { Observable } from 'rxjs';
import { AppService } from '@appServices/app.service';
import { UserOptionsComponent } from '@components/user-options/user-options.component';
import { CdkOverlayOrigin, Overlay } from '@angular/cdk/overlay';
import { PopoverService } from '@appServices/popover.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'user-action',
  templateUrl: './user-action.component.html',
  styleUrls: ['./user-action.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserActionComponent implements OnInit, AfterViewChecked {
  public user$: Observable<Partial<UserGraph>>;
  private _key = 'user';
  @ViewChild(CdkOverlayOrigin) _overlayOrigin: CdkOverlayOrigin;

  constructor(
    private _appService: AppService,
    private overlay: Overlay,
    private _popoverService: PopoverService
  ) {}

  ngOnInit() {
    this._appService.getUser(`userName avatar`).subscribe();
    this._popoverService.setPositionStrategy('relative');
    this.user$ = this._appService.graphUser$.pipe(
      map(({ userName, avatar }) => ({ userName, avatar }))
    );
  }

  ngAfterViewChecked() {
    this._popoverService.setAttachComponent(UserOptionsComponent, this._key);
    this._popoverService.setPortalOrigin(this._overlayOrigin, this._key);
  }

  public toggle() {
    this._popoverService.update(this._key);
  }
}
